module PSR(input Clk , Nin , Zin , Cin , Vin , Ld , output reg N , Z , C , V);
initial N <= 1'b0;
initial Z <= 1'b0;
initial C <= 1'b0;
initial V <= 1'b0;
always @ (posedge Clk , Ld)
begin
  if(Ld)
  begin
  N = Nin;
  Z = Zin;
  C = Cin ;
  V = Vin;
  
  // $display("PSR NZCV: %b%b%b%b",N , Z , C , V);
  end
end


endmodule
