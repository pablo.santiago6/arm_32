module MDR(input[31:0]Ds , input Ld , Clk , output reg[31:0]Qs);
    initial Qs <= 0;
    always @(posedge Clk , Ld)
    begin
    
        if(Ld)begin
         Qs <= Ds;
        // $display("MDR  Qs: %b",Qs);
        end
        
    end


endmodule