`define BYTE 2'b00
`define WORD 2'b10
`define HALFWORD 2'b01
`define START_ADDRESS 8'b00000000


module ram_256_b(input [31:0]data_input, input [7:0]address,
                 input w_r_mode, input enable, input [1:0] op_code,
                 output reg MOC, output reg[31:0] mem , input[31:0] IR);
    // reg [31:0] storage[0:255]; 
    reg[7:0] storage[0:255];//512 bytes
    //integer fo, file_descriptor;
    integer i;
    reg[1:0] OP_CODE;
	
    always @(posedge enable, w_r_mode, address) begin
      
      		MOC <= 0;
       
      if (enable == 1)//High -> we are being requested
                begin
                  
                 //Need to find out which mode it is , and find when to use this 
                /* 
                  if(IR)
                 
                  case(IR[27:26])
                    
                    2'b01: // Adressing mode 2
            			begin
                         // $display("Addressing mode 2");
                          OP_CODE <= (IR[20])? `BYTE : `WORD;
                        end
                    
                   2'b00:begin
                      $display("Addressing mode 3");
                      case({IR[20] , IR[6] , IR[5] })
                        
                        3'b001: begin
                          OP_CODE <= `HALFWORD;
                        end
                        
                        3'b010: begin
                          OP_CODE <= `WORD; //TODO Change to double word
                        end
                        
                        3'b011: begin
                          OP_CODE <= `WORD; //TODO Change to double word
                        end
                        
                        3'b101: begin
                        	OP_CODE <= `BYTE;
                        end
                        
                        3'b110: begin
                          OP_CODE <= `BYTE;
                        end
                        
                        3'b111: begin
                          OP_CODE <= `BYTE;
                        end
                        
                        default:begin
                          OP_CODE <= `WORD;
                        end
                      endcase
                      
                    end
                      
                    default: OP_CODE <= `WORD;
                    
                  endcase
                  else OP_CODE <= `WORD;
                    
                  
                 
                  */
                    
                    if(w_r_mode)begin//HIGH -> read mode
                      case(op_code)
                            `BYTE://byte
                                begin
                                    mem[7:0] <= storage[address];
                                    mem[31:8] <= 24'h000000;
                                  $display("Byte Read %b  at address: %b" , mem , address);
                                end
                            
                            `HALFWORD://// halfword(16-bit)
                                begin
                                  
                                    mem[15:8] <= storage[address];
                                    mem[7:0] <= storage[address + 1];
                                    mem[31:16] <= 16'h0000;//padding the rest
                                //  $display("Halfword Read %d  at address: %d" , mem , address);
                                end
                          
                            `WORD:  // word (32-bit)
                                begin
                                //    for(i = 0; i < 256 ; i++)
                                    //    $display("Storage[%d]: %b"  , address , storage[address]);
                                  
                                    mem[31:24] <= storage[address];
                                    mem[23:16] <= storage[address+1];
                                    mem[15:8] <= storage[address+2];
                                    mem[7:0] <= storage[address+3];
                                  //$display("Word Read %d  at address: %d" , mem , address);
                                    //state = 1'b1;
                                end

			            //last case is don't care
                        endcase
                        MOC <= 1;
                    end

                    else //write mode
                    begin
                        MOC<=0;
                        case(op_code)

                            // Byte
                            `BYTE:
                            begin
                                // storage[address+3] = 8'b00000000;
                                // storage[address+2] <= 8'b00000000;
                                // storage[address+1] <= 8'b00000000;
				                storage[address] <= data_input[7:0];
                              $display("Byte Write %d at address %d", storage[address] , address);
                            end

                            // Halfword
                            `HALFWORD:
                                begin
                                    storage[address] <= data_input[15:8];
                                    storage[address + 1] <= data_input[7:0];
                                    //state = 1'b1;
                                end

                            // Word
                            `WORD:
                                begin
                                    storage[address] <= data_input[31:24];
                                    storage[address+1] <= data_input[23:16];
                                    storage[address+2] <= data_input[15:8];
                                    storage[address+3] <= data_input[7:0];
                                end

                            // default: begin
				            //      storage[address] = data_input[7:0];
                            // end

                        endcase
                        MOC<=1;
                        // MOC <=0;
                    end

                end

            //MOC =1; 
             //$display("Time: %d  RAM  enable: %b     R_W:  %b  mode: %b  address: %b  Read: %b", $time , enable , w_r_mode , OP_CODE , address , mem);
            

            MOC <= 1;
        end




endmodule

    
module ram_interact(input[31:0] data , input[7:0] address , input   w_r , enable , input[1:0] mode , output MOC , output[31:0] data_output , input[31:0] IR);
    integer file_in, file_out, file_status, i;
    // reg [31:0] data;
	// reg enable, w_r;
    reg [7:0] data_in;
	reg [7:0] addresser;
    // reg[1:0] mode;
    wire [31:0] data_output;
	wire MOC;
	
  ram_256_b ram(data, address, w_r, enable, mode, MOC,data_output ,  IR);
	
    initial begin
		file_in = $fopen("data_bin.txt","rb");
        //file_in = $fopen("data.txt","r");
        // w_r = 1'b0;
        // mode = `WORD;
		addresser =  8'b00000000;
		while (!$feof(file_in)) begin
			file_status = $fscanf(file_in, "%b", data_in);
            //file_status = $fread(data, file_in);
            //$readmemb(file_in, data);
			ram.storage[addresser] = data_in;
			// $display("Memory address: %h \n Data: %b", addresser, data_in);
			addresser = addresser + 1;
		end
		$fclose(file_in);
	end

	// initial begin
    //     $display("-------------------------------------------------------");
    //     $display("\tADDRESS\t\t|\tVALUE\t\t|\tTime\t|\tENABLE\t|\tMOC\t|\tR/W");
    //     $display("-------------------------------------------------------");
    //     mode  = `WORD;
	// 	enable = 1'b0; w_r = 1'b1;
	// 	address =  8'b00000000;

    //     repeat(4) begin
    //     #5 enable = 1'b1;//one tick
    //     #5 enable = 1'b0;

    //         begin
    //             case(mode)
    //                 `WORD:
    //                     begin
    //                         address =address + 4;
    //                     end
                    
    //                 `HALFWORD:
    //                     begin
    //                         address = address + 2;
    //                     end
                    
    //                 `BYTE:
    //                     begin
    //                         address = address + 1;
    //                     end
    //             endcase
    //         end


    //     end

    // end

    //     always@(posedge enable)
    //     begin
    //         #1 $display(" \t\t%h\t\t | %h\t |\t\t%b\t\t|\t%b\t|\t %b\t", address,data_output,enable, MOC,w_r);
    //     end


//         initial begin
//             #70;
//             #5 $display("\t\t Enable\t MOC\t W/R");
//             address = 0;
//             enable=0;
//             enable=1;
//             w_r=0;
//             mode = `BYTE;
//             data_in = 8'b11000011;

//             #10;
//             $display("-------------------------------------------------------");
//             $display("\n Write byte %b to ram", data_in);
//             $display("\n Address %h : %b\t%b\t%b\t\t%b", address,data_output,enable,w_r,MOC);
//             $display("-------------------------------------------------------");
            
//             address = 1;
//             enable=0;
//             enable=1;
//             w_r=0;
//             mode = `HALFWORD;
//             data_in = 16'hCACA;

//             #10;
//             $display("-------------------------------------------------------");
//             $display("\n Write halfword %h to ram", data_in);
//             $display("\n Address %h : %h\t%b\t%b\t\t%b", address,data_output,enable,w_r,MOC);
//             address = address + 1;
//             $display("\n Address %h : %h\t%b\t%b\t\t%b", address,data_output,enable,w_r,MOC);
//             $display("-------------------------------------------------------");
            
//             address = 3;
//             enable=0;
//             enable=1;
//             w_r=0;
//             mode = `HALFWORD;
//             data_in = 16'hBABA;

//             #10;
//             $display("-------------------------------------------------------");
//             $display("\n Write halfword %h to ram", data_in);
//             $display("\n Address %h : %h\t%b\t%b\t\t%b", address,ram.storage[address],enable,w_r,MOC);
//             address = address + 1;
//             $display("\n Address %h : %h\t%b\t%b\t\t%b", address,ram.storage[address],enable,w_r,MOC);            
//             $display("-------------------------------------------------------");

//             address = 7;
//             enable=0;
//             enable=1;
//             w_r=0;
//             mode = `WORD;
//             data_in = 32'hCACABABA;

//             #20;
//             $display("-------------------------------------------------------");
//             $display("\n Write word %h to ram", data_in);
//             $display("\n Address %h : %h\t%b\t%b\t\t%b", address,data_output,enable,w_r,MOC);
//             address = address + 1;
//             $display("\n Address %h : %h\t%b\t%b\t\t%b", address,data_output,enable,w_r,MOC);            
//             address = address + 1;
//             $display("\n Address %h : %h\t%b\t%b\t\t%b", address,data_output,enable,w_r,MOC);            
//             address = address + 1;
//             $display("\n Address %h : %h\t%b\t%b\t\t%b", address,data_output,enable,w_r,MOC);            
//             $display("-------------------------------------------------------");

//             $display("\n\t\t\t ENABLE\tMOC\tW/R");
            
            
// 		enable=0;enable=1;
// 		w_r=1;
// 		mode = `BYTE;
// 		address=0;		
// 		#10
// 		$display("\nReading Byte in Address 0.\nData is: %b\t\t%b\t%b\t%b", data_output, enable, w_r, MOC);

// 		enable=0;enable=1;
// 		w_r=1;
// 		mode= `HALFWORD;
// 		address=2;
// 		#10
// 		$display("\nReading Halfword in Address 2.\nData is: %b\t\t%b\t%b\t%b", data_output, enable, w_r, MOC);

// 		enable=0;enable=1;
// 		w_r=1;
// 		mode= `HALFWORD;
// 		address=4;
// 		#10
// 		$display("\nReading Halfword in Address 4.\nData is: %b\t\t%b\t%b\t%b", data_output, enable, w_r, MOC);

// 		enable=0;
//         enable=1;
// 		w_r=1;
// 		mode=`WORD;
// 		address = 8;
// 		#10;
// 		$display("\nReading Word in Address 8.\nData is: %b\t\tb\t%b\t%b", data_output, enable, w_r, MOC);
		
// 		// enable=0;enable=1;
// 		// w_r=1;
// 		// mode=`WORD;
// 		// address = 0;
// 		// #10;
// 		// $display("\nReading Word in Address 0.\nData is: %b\t\t%b\t %b\t%b", data_output, enable, w_r, MOC);
		
// 		// enable=0;
//         // enable=1;
// 		// w_r=1;
// 		// mode=`WORD;
// 		// address = 4;
// 		// #10;
// 		// $display("\nReading Word in Address 4.\nData is: %b\t\t%b\t%b\t%b", data_output, enable, w_r, MOC);
		
// 		// enable=0;
//         // enable=1;
// 		// w_r=1;
// 		// mode=`WORD;
// 		// address = 8;
// 		// #10;
// 		// $display("\nReading Word in Address 8.\nData is: %b\t\t%b\t%b\t %b", data_output, enable, w_r, MOC);
		
// 		$finish;
//         end

// endmodule
endmodule

