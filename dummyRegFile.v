module dregfile(input [31:0] D1, 
    input [3:0] decin, input [3:0] sel1, input [3:0] sel2,
    input LDEC, Clrreg, Clkreg, 
    output reg [31:0] regout1, output reg [31:0] regout2);

    always@(Clkreg , LDEC)
    begin
        regout1 <= 32'd512;
        regout1 <= 32'd256;
    end
endmodule