`include "control_unit.v"
`include "Data_Path.v"
module ARM32(input Clk , Clr);


wire FR,RF, IR_ld ,MDR,MAR,R_W,MOV,MA_1,MA_0,MB_1,MB_0,MC_1,MC_0,MD, ME, OP4,OP3,OP2,OP1,OP0;
wire Moc;
wire Cond;
wire[31:0] InstructionRegister;

ControlUnit cu( FR,RF,MDR,MAR,R_W,MOV,MA_1,MA_0,MB_1,MB_0,MC_1,MC_0,MD, ME, OP4,OP3,OP2,OP1,OP0,IR_ld ,InstructionRegister,  Moc, Cond, Clr, Clk );
DataPath DP    (FR,RF,MDR,MAR,R_W,MOV,MA_1,MA_0,MB_1,MB_0,MC_1,MC_0,MD, ME, OP4,OP3,OP2,OP1,OP0,IR_ld , InstructionRegister ,Moc ,Cond ,Clr , Clk );




endmodule


// Code your testbench here
// or browse Examples
`include "ARM_32.v"
module test();

reg Clk , Clr;
ARM32 my(Clk , Clr);
// parameter sim_time  = 1000;
integer address;
initial begin

Clk =   1'b1;

Clr = 1'b0;
#5 Clr = 1'b0;

 #10 Clr = 1'b1;
//  #15 Clr = 1'b0;


#5		
begin
    // $display("Moc: %b ", my.Moc);

  repeat(1000) #100 Clk = ~Clk; 
  for(address = 0 ; address < 256 ; address = address + 4)
    if(my.DP.ram.ram.storage[address])$display(" Address: %d  | %d %d %d %d ", address , my.DP.ram.ram.storage[address],my.DP.ram.ram.storage[address+1],my.DP.ram.ram.storage[address+ 2],my.DP.ram.ram.storage[address+3]);
    
end
end
initial begin
 
$monitor("\n Time:%d \n State: %d FR: %b  RF: %b MDR: %b MAR: %b R_W: %b MOV: %b MA_1: %b MA_0: %b MB_1: %b MB_0: %b  MC_1: %b MC_0: %b  MD: %b  ME: %b  OP: %b%b%b%b%b   IR_ld: %b  IR: %b %b %b %b Moc: %b   Cond: %b  Clr: %b \n alu: A: %d   B: %d   OP: %b  Out: %d \n RAM: data: %b    address: %b    w_r: %b  enable:%b   mode: %b   out: %b  \n RegFile: Data In: %d   SaveTo: %d  regA:%d  regB:%d   PC:%d   Load: %d \n PSR: N:%b V:%b Z:%b C:%b "  ,$time , my.cu.CS , my.FR,my.RF,my.MDR,my.MAR,my.R_W,my.MOV,my.MA_1,my.MA_0,my.MB_1,my.MB_0,my.MC_1,my.MC_0,my.MD, my.ME, my.OP4,my.OP3,my.OP2,my.OP1,my.OP0,my.IR_ld ,my.InstructionRegister[31:24],my.InstructionRegister[23:16],my.InstructionRegister[15:8],my.InstructionRegister[7:0]  ,my.Moc, my.Cond, my.Clr , my.DP.A , my.DP.B ,  my.DP.OPs ,  my.DP.out ,my.DP.ram.data, my.DP.ram.address, my.DP.ram.w_r, my.DP.ram.enable, my.DP.ram.ram.op_code,my.DP.dataOut,my.DP.freg.pc,my.DP.freg.bd,my.DP.freg.ma ,my.DP.freg.mb,my.DP.freg.wr15 , my.DP.freg.rfld , my.DP.psr.N , my.DP.psr.V , my.DP.psr.Z , my.DP.psr.C);
  end



endmodule
