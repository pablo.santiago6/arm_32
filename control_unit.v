`define BYTE 2'b00
`define WORD 2'b10
`define HALFWORD 2'b01

module ControlUnit(output FR,RF,output MDR,MAR,R_W,MOV,MA_1,MA_0,MB_1,MB_0,MC_1,MC_0,MD, ME, OP4,OP3,OP2,OP1,OP0,IR_ld, input[31:0] IR ,input wire Moc, Cond, Clr, Clk , output[1:0]OP_CODE);

	
	wire[7:0] NS, CS ;
	StateReg SR(CS , NS , Clr , Clk);
	NextStateDecoder NSD( NS , IR[31:0], CS , Cond , Moc);
	ControlSignalsEncoder SE( FR , RF , IR[31:0] , MDR , MAR , R_W , MOV , MA_1 , MA_0 , MB_1 , MB_0 , MC_1 , MC_0 , MD , ME , OP4 , OP3 , OP2 , OP1 , OP0 ,  IR_ld ,OP_CODE, CS  );

endmodule


module NextStateDecoder(output reg [7:0] NextState, input[31:0] IR, input[7:0] State,  input Cond ,Moc);

  //reg[2:0] checker;
	// initial NextState<=8'd0;
	
    always@*
	
	begin


	
    case(State)

        8'd0: begin NextState<=8'd1;end  

        8'd1: begin NextState<=8'd2;end

        8'd2 :begin NextState<=8'd3;end

        8'd3: begin 
				if( Moc ) NextState<=8'd4;
                else NextState <= 8'd3;
             end
		
		8'd4: begin
		if(Cond == 1)
		begin
		  case(IR[27:25])
			3'b000: begin // Data Processing Imm Shift or Data Processing reg
			  //TODO check for extra load/store

			if(IR[4] == 0)
			  NextState<=(IR[4] != 1) ?  8'd7 : 8'd6; 
			else
			
			begin // LOAD/ Store Adressing mode 3
              
              if(IR[20] == 1) begin
                if(IR[24] == 1) //offset o pre index
                begin
                  case(IR[22:21])

                      2'b00:begin
                        //reg offset 
                        NextState<=8'd98;
                      end

                      2'b01:begin
                        //reg pre
                        NextState<=8'd100;
                      end

                      2'b10:begin
                        //imm offset
                        NextState<=8'd95;
                      end

                      2'b11:begin
                        // Imm pre
                        NextState<=8'd97;
                      end

                  endcase
                end
                else begin
                  NextState <= (IR[22] == 1)? 8'd96:8'd99; // Posts 
                end
              end
              else begin //store
                if(IR[24] == 1)
                begin
                  case(IR[22:21])

                      2'b00:begin
                        //reg offset 
                        NextState<=8'd126;
                      end

                      2'b01:begin
                        //reg pre
                        NextState<=8'd128;
                      end

                      2'b10:begin
                        //imm offset
                        NextState<=8'd123;
                      end

                      2'b11:begin
                        // Imm pre
                        NextState<=8'd125;
                      end

                  endcase
                end
              
              else begin
                NextState <= (IR[22] == 1)? 8'd124:8'd127;
                end
     

			end
		end
            end

			3'b001: begin // Data Processing Imm or move to SR 
			  NextState<=8'd5;
			end
//-----------------Adressing Mode 2 ---------------------
//TODO check how im going to pass the signals to the RAM
//TODO add state trans for stores

		
			3'b010: begin // Load/Store imm offset  
			  //TODO check for pre/post 

			  if(IR[20] == 1)begin  // Load
				if(IR[24] == 0) NextState<=8'd9; // Post
				else
				begin
					NextState<=(IR[21] == 1) ? 8'd10 : 8'd8;
				end
			  end

			  else begin
				
				if(IR[24] == 0) NextState<=8'd51; // Post
				else
				begin
					NextState<=(IR[21] == 1) ? 8'd52 : 8'd50;
				end
			  end
			  
			end

			3'b011: begin // Load/Store Reg Offset or scaled reg 
			// I think i can send them to the same thing 
              $display("IR20 %d ", IR[20]);
              if(IR[20] == 1)begin //load 
				if(IR[24] == 0) NextState<=8'd12; // Post
				else
				begin
					NextState<=(IR[21] == 1) ? 8'd13 : 8'd11;
				end
			  end

			  else begin
				if(IR[24] == 0) NextState<=8'd54; // Post
				else
				begin
					NextState<=(IR[21] == 1) ? 8'd55 : 8'd53;
				end
			  end
			end
//------------------------------------End of Adressing Mode 2 Total: 9--------

//------------------------------------Adressing Mode 3 ------------------------

			3'b101:begin
              if(IR[24]) NextState <= 200; 
			  else NextState <= 92;
			end


			default: begin
			  NextState<=8'd4;
			  
			end


		  endcase
		  end
		  else NextState<=8'd1;
		end


		8'd5:begin
		  NextState<=8'd1;
		end

		8'd6:begin
		  NextState<=8'd1;
		end

		8'd7:begin
		  NextState<=8'd1;
		end

		8'd8:begin
		  NextState<=8'd17;
		end

		8'd9:begin
		  NextState<=8'd18;
		end

		8'd10:begin
		  NextState<=8'd19;
		end

		8'd11:begin
		  NextState<=8'd20;
		end

		8'd12:begin
		  NextState<=8'd21;
		end

		8'd13:begin
		  NextState<=8'd22;
		end

		8'd14:begin
		  NextState<=8'd23;
		end

		8'd15:begin
		  NextState<=8'd24;
		end

		8'd16:begin
		  NextState<=8'd25;
		end
		

		8'd17: begin
		  NextState<=8'd26;
		end

		8'd18: begin
		  NextState<=8'd27;
		end

		8'd19: begin
		  NextState<=8'd29;
		end

		8'd20:begin
		  NextState <= 8'd1;
		end

		8'd21: begin
		  NextState<=8'd30;
		end

		8'd22: begin
		  NextState<=8'd31;
		end

		8'd23: begin
		  NextState<=8'd32;
		end

		8'd24: begin
		  NextState<=8'd33;
		end

		8'd25: begin
		  NextState<=8'd34;
		end

		8'd26: begin
		  NextState<=(Moc == 1) ? 8'd35 : 8'd26;
		end

		8'd27: begin
		  NextState<=(Moc == 1) ? 8'd36 : 8'd27;
		end

		8'd28: begin
		  NextState<=(Moc == 1) ? 8'd37 : 8'd28;
		end

		8'd29: begin
		  NextState<=(Moc == 1) ? 8'd38 : 8'd29;
		end
		
		8'd30: begin
		  NextState<=(Moc == 1) ? 8'd39 : 8'd30;
		end

		8'd31: begin
		  NextState<=(Moc == 1) ? 8'd40 : 8'd31;
		end

		8'd32: begin
		  NextState<=(Moc == 1) ? 8'd41 : 8'd32;
		end

		8'd33: begin
		  NextState<=(Moc == 1) ? 8'd42 : 8'd33;
		end

		8'd34: begin
		  NextState<=(Moc == 1) ? 8'd43 : 8'd34;
		end
        
		8'd35: begin
			NextState<=8'd1;
		end

		8'd36: begin
			NextState<=8'd44;
		end

		8'd37: begin
			NextState<=8'd45;
		end

		8'd38: begin
			NextState<=8'd1;
		end

		8'd39:begin
			NextState<=8'd46;
		end

		8'd40: begin
			NextState<=8'd47;
		end

		8'd41: begin
			NextState<=8'd1;
		end

		8'd42: begin
			NextState<=8'd48;
		end

		8'd43: begin
			NextState<=8'd49;
		end

		8'd44: begin
			NextState<=8'd1;
		end

		8'd45: begin
			NextState<=8'd1;
		end

		8'd46: begin
			NextState<=8'd1;
		end

		8'd47: begin
			NextState<=8'd1;
		end

		8'd48: begin
			NextState<=8'd1;
		end

		8'd49: begin
			NextState<=8'd1;
		end

		8'd50: begin
			NextState<=8'd59;
		end

		8'd51: begin
			NextState<=8'd60;
		end

		8'd52: begin
			NextState<=8'd61;
		end

		8'd53: begin
			NextState<=8'd62;
		end

		8'd54: begin
			NextState<=8'd63;
		end

		8'd55: begin
			NextState<=8'd64;
		end

		8'd56: begin
			NextState<=8'd65;
		end

		8'd57: begin
			NextState<=8'd66;
		end

		8'd58: begin
			NextState<=8'd67;
		end

		//59

		8'd59: begin
			NextState<=8'd68;
		end

		8'd60: begin
			NextState<=8'd69;
		end

		8'd61: begin
			NextState<=8'd70;
		end

		8'd62: begin
			NextState<=8'd71;
		end

		8'd63: begin
			NextState<=8'd72;
		end

		8'd64: begin
			NextState<=8'd73;
		end

		8'd65: begin
			NextState<=8'd74;
		end

		8'd66: begin
			NextState<=8'd75;
		end

		8'd67: begin
			NextState<=8'd76;
		end

		//68
		8'd68: begin
			NextState<=8'd77;
		end

		8'd69: begin
			NextState<=8'd78;
		end

		8'd70: begin
			NextState<=8'd79;
		end

		8'd71: begin
			NextState<=8'd80;
		end

		8'd72: begin
			NextState<=8'd81;
		end

		8'd73: begin
			NextState<=8'd82;
		end

		8'd74: begin
			NextState<=8'd83;
		end

		8'd75: begin
			NextState<=8'd84;
		end

		8'd76: begin
			NextState<=8'd85;
		end


		//77
		8'd77: begin
		  NextState<=(Moc == 1) ? 8'd1 : 8'd77;
		end

		8'd78: begin
		  NextState<=(Moc == 1) ? 8'd86 : 8'd78;
		end

		8'd79: begin
		  NextState<=(Moc == 1) ? 8'd87 : 8'd79;
		end

		//NEEDS TO LD Reg Offset
		8'd80: begin
		  NextState<=(Moc == 1) ? 8'd1 : 8'd80;
		end

		8'd81: begin
		  NextState<=(Moc == 1) ? 8'd88 : 8'd81;
		end

		8'd82: begin
		  NextState<=(Moc == 1) ? 8'd89 : 8'd82;
		end

		8'd83: begin
		  NextState<=(Moc == 1) ? 8'd1 : 8'd83;
		end

		8'd84: begin
		  NextState<=(Moc == 1) ? 8'd90 : 8'd84;
		end

		8'd85: begin
		  NextState<=(Moc == 1) ? 8'd91 : 8'd85;
		end

		8'd86: begin
		  NextState<=8'd1;
		end

		8'd87: begin
		  NextState<=8'd1;
		end

		8'd88: begin
		  NextState<=8'd1;
		end

		8'd89: begin
		  NextState<=8'd1;
		end

		8'd90: begin
		  NextState<=8'd1;
		end

		8'd91: begin
		  NextState<=8'd1;
		end

		//92
		8'd92: begin
			NextState<=8'd1;
		end

		8'd93: begin
			NextState<=8'd1;
		end

		8'd94: begin
			NextState<=8'd1;
		end

		8'd95: begin
			NextState<=8'd101;
		end

		8'd96: begin
			NextState<=8'd102;
		end

		8'd97: begin
			NextState<=8'd103;
		end

		8'd98: begin
			NextState<=8'd104;
		end
		
		8'd99: begin
			NextState<=8'd105;
		end

		8'd100: begin
			NextState<=8'd106;
		end

		8'd101: begin
			NextState<=8'd107;
		end
		
		8'd102: begin
			NextState<=8'd108;
		end

		8'd103: begin
			NextState<=8'd109;
		end

		8'd104: begin
			NextState<=8'd110;
		end

		8'd105: begin
			NextState<=8'd111;
		end

		8'd106: begin
			NextState<=8'd112;
		end

		8'd107: begin
			NextState<=(Moc == 1) ? 8'd113  : 8'd107; 
		end
		
		8'd108: begin
			NextState<= (Moc == 1) ? 8'd114 : 8'd108;
		end

		8'd109: begin
			NextState<= (Moc == 1) ? 8'd115 : 8'd109;
		end

		8'd110: begin
			NextState<= (Moc == 1) ? 8'd116 : 8'd110;
		end

		8'd111: begin
			NextState<= (Moc == 1) ? 8'd117 : 8'd111;
		end

		8'd112: begin
			NextState<= (Moc == 1) ? 8'd118 : 8'd112;
		end

		8'd113: begin
			NextState<= 8'd1;
		end

		8'd114: begin
			NextState<= 8'd119;
		end

		8'd115: begin
			NextState<= 8'd120;
		end

		8'd116: begin
			NextState<= 8'd1;
		end

		8'd117: begin
			NextState<= 8'd121;
		end

		8'd118: begin
			NextState<= 8'd122;
		end

		8'd119: begin
			NextState<= 8'd1;
		end

		8'd120: begin
			NextState<= 8'd1;
		end

		8'd121: begin
			NextState<= 8'd1;
		end

		8'd122: begin
			NextState<= 8'd1;
		end

		//123
		8'd123: begin
			NextState<=8'd129;
		end

		8'd124: begin
			NextState<=8'd130;
		end

		8'd125: begin
			NextState<=8'd131;
		end

		8'd126: begin
			NextState<=8'd132;
		end

		8'd127:begin
			NextState<=8'd133;
		end

		8'd128:begin
			NextState<=8'd134;
		end

		//129
		8'd129: begin
			NextState<=8'd135;
		end
		
		8'd130: begin
			NextState<=8'd136;
		end

		8'd131: begin
			NextState<=8'd137;
		end

		8'd132: begin
			NextState<=8'd138;
		end

		8'd133: begin
			NextState<=8'd139;
		end

		8'd134: begin
			NextState<=8'd140;
		end

		//135
		8'd135: begin
			NextState<= 8'd141;
		end

		8'd136: begin
			NextState<= 8'd142;
		end

		8'd137: begin
			NextState<= 8'd143;
		end

		8'd138: begin
			NextState<= 8'd144;
		end

		8'd139: begin
			NextState<= 8'd145;
		end

		8'd140: begin
			NextState<= 8'd146;
		end

		//141
		8'd141: begin
			NextState<= (Moc == 1) ? 8'd1 : 8'd141;
		end

		8'd142: begin
			NextState<= (Moc == 1) ? 8'd147 : 8'd142;
		end

		8'd143: begin
			NextState<= (Moc == 1) ? 8'd148 : 8'd143;
		end

		8'd144: begin
			NextState<= (Moc == 1) ? 8'd1 : 8'd144;
		end

		8'd145: begin
			NextState<= (Moc == 1) ? 8'd149 : 8'd145;
		end

		8'd146: begin
			NextState<= (Moc == 1) ? 8'd150 : 8'd146;
		end

		8'd147: begin
			NextState<=  8'd1;
		end

		8'd148: begin
			NextState<=  8'd1;
		end

		8'd149: begin
			NextState<=  8'd1;
		end

		8'd150: begin
			NextState<=  8'd1;
		end
      
      	8'd200: begin
          NextState <=9'd92;
        end

        default:begin  NextState<=8'd0; end

    endcase

	end

endmodule

module ControlSignalsEncoder( output reg FR_ld,RF_ld,output [31:0]IR, output reg MDR_ld,MAR_ld,R_W,MOV,MA1,MA0,MB1,MB0,MC1,MC0,MD, ME, OP4,OP3,OP2,OP1,OP0, IR_ld, output reg[1:0] OP_CODE ,  input [7:0]State);
		
		initial	FR_ld <= 0;
		initial	RF_ld <= 0;
		initial		IR_ld <= 0;
		initial		MAR_ld <= 0;
		initial		MDR_ld <= 0;
		initial		R_W <= 0;
		initial		MOV <= 0;
		initial		MA1 <= 0;
		initial		MA0 <= 0;
		initial		MB1 <= 0;
		initial		MB0 <= 0;
		initial		MC1 <= 0;
		initial		MC0 <= 0;
		initial		MD <= 0;
		initial		ME <= 0;
		initial		OP4 <= 0;
		initial		OP3 <= 0;
		initial		OP2 <= 0;
		initial		OP1 <= 0;
		initial		OP0 <= 0;	

		
    always@*
		begin 
          OP_CODE<=`WORD;
		case(State)
			8'd0:
			begin
				FR_ld <= 0;
				RF_ld <= 1;
				IR_ld <= 0;
				MAR_ld <= 0;
				MDR_ld <= 0;
				R_W <= 0;
				MOV <= 0;
				MA1 <= 0;
				MA0 <= 0;
				MB1 <= 1;
				MB0 <= 1;
				MC1 <= 0;
				MC0 <= 1;
				MD <= 1;
				ME <= 0;
				OP4 <= 0;
				OP3 <= 1;
				OP2 <= 1;
				OP1 <= 0;
				OP0 <= 1;
				// $display("State 0 %b%b%b%b%b" , OP4, OP3 , OP2 , OP1, OP0);
			end
			8'd1:
			begin
				FR_ld <= 0;
				RF_ld <= 0;
				IR_ld <= 0;
				MAR_ld <= 1;
				MDR_ld <= 0;
				R_W <= 0;
				MOV <= 0;
				MA1 <= 1;
				MA0 <= 0;
				MB1 <= 0;
				MB0 <= 0;
				MC1 <= 0;
				MC0 <= 0;
				MD <= 1;
				ME <= 0;
				OP4 <= 1;
				OP3 <= 0;
				OP2 <= 0;
				OP1 <= 0;
				OP0 <= 0;
				// $display("State 1 %b%b%b%b%b" , OP4, OP3 , OP2 , OP1, OP0);
			end
			8'd2:
			begin
				
				FR_ld <= 0;
				RF_ld <= 1;
				IR_ld <= 0;
				MAR_ld <= 0;
				MDR_ld <= 0;
				R_W <= 1;
				MOV <= 1;
				MA1 <= 1;
				MA0 <= 0;
				MB1 <= 0;
				MB0 <= 0;
				MC1 <= 0;
				MC0 <= 1;
				MD <= 1;
				ME <= 0;
				OP4 <= 1;
				OP3 <= 0;
				OP2 <= 0;
				OP1 <= 0;
				OP0 <= 1;
				// $display("State 2 %b%b%b%b%b" , OP4, OP3 , OP2 , OP1, OP0);
			end
			8'd3:
			begin
				FR_ld <= 0;
				RF_ld <= 0;
				IR_ld <= 1;
				MAR_ld <= 0;
				MDR_ld <= 0;
				R_W <= 1;
				MOV <= 1;
				MA1 <= 0;
				MA0 <= 0;
				MB1 <= 0;
				MB0 <= 0;
				MC1 <= 0;
				MC0 <= 0;
				MD <= 0;
				ME <= 0;
				OP4 <= 0;
				OP3 <= 0;
				OP2 <= 0;
				OP1 <= 0;
				OP0 <= 0;
			end
			8'd4:
			begin
				FR_ld <= 0;
				RF_ld <= 0;
				IR_ld <= 0;
				MAR_ld <= 0;
				MDR_ld <= 0;
				R_W <= 0;
				MOV <= 0;
				MA1 <= 0;
				MA0 <= 0;
				MB1 <= 0;
				MB0 <= 0;
				MC1 <= 0;
				MC0 <= 0;
				MD <= 0;
				ME <= 0;
				OP4 <= 0;
				OP3 <= 0;
				OP2 <= 0;
				OP1 <= 0;
				OP0 <= 0;
			end
			8'd5:
			begin
              FR_ld <= (IR[20] == 1) ? 1:0;
				RF_ld <= 1;
				IR_ld <= 0;
				MAR_ld <= 0;
				MDR_ld <= 0;
				R_W <= 0;
				MOV <= 0;
				MA1 <= 0;
				MA0 <= 0;
				MB1 <= 0;
				MB0 <= 1;
				MC1 <= 0;
				MC0 <= 0;
				MD <= 0;
				ME <= 0;
				OP4 <= 0;
				OP3 <= 0;
				OP2 <= 0;
				OP1 <= 0;
				OP0 <= 0;
			end
			8'd6:
			begin
				FR_ld <= (IR[20] == 1) ? 1:0;
				RF_ld <= 1;
				IR_ld <= 0;
				MAR_ld <= 0;
				MDR_ld <= 0;
				R_W <= 0;
				MOV <= 0;
				MA1 <= 0;
				MA0 <= 0;
				MB1 <= 0;
				MB0 <= 0;
				MC1 <= 0;
				MC0 <= 0;
				MD <= 0;
				ME <= 0;
				OP4 <= 0;
				OP3 <= 0;
				OP2 <= 0;
				OP1 <= 0;
				OP0 <= 0;
			end
			8'd7:
			begin
				FR_ld <= (IR[20] == 1) ? 1:0;
				RF_ld <= 1;
				IR_ld <= 0;
				MAR_ld <= 0;
				MDR_ld <= 0;
				R_W <= 0;
				MOV <= 0;
				MA1 <= 0;
				MA0 <= 0;
				MB1 <= 0;
				MB0 <= 1;
				MC1 <= 0;
				MC0 <= 0;
				MD <= 0;
				ME <= 0;
				OP4 <= 0;
				OP3 <= 0;
				OP2 <= 0;
				OP1 <= 0;
				OP0 <= 0;
			end
			8'd8:
			begin
				FR_ld <= 0;
				RF_ld <= 0;
				IR_ld <= 0;
				MAR_ld <= 0;
				MDR_ld <= 1;
				R_W <= 0;
				MOV <= 0;
				MA1 <= 0;
				MA0 <= 0;
				MB1 <= 0;
				MB0 <= 1;
				MC1 <= 0;
				MC0 <= 0;
				MD <= 1;
				ME <= 0;
				OP4 <= 0;
				OP3 <= 0;
				OP2 <= (IR[20]) ? 1 : 0;
				OP1 <= (IR[20]) ? 0 : 1;
				OP0 <= 0;
              OP_CODE <= (IR[22])? `BYTE : `WORD;
			end
			8'd9:
			begin
				FR_ld <= 0;
				RF_ld <= 0;
				IR_ld <= 0;
				MAR_ld <= 0;
				MDR_ld <= 1;
				R_W <= 0;
				MOV <= 0;
				MA1 <= 0;
				MA0 <= 0;
				MB1 <= 0;
				MB0 <= 0;
				MC1 <= 0;
				MC0 <= 0;
				MD <= 1;
				ME <= 0;
				OP4 <= 1;
				OP3 <= 0;
				OP2 <= (IR[20]) ? 1 : 0;
				OP1 <= (IR[20]) ? 0 : 1;
				OP0 <= 0;
              	 OP_CODE <= (IR[22])? `BYTE : `WORD;
			end
			8'd10:
			begin
				FR_ld <= 0;
				RF_ld <= 0;
				IR_ld <= 0;
				MAR_ld <= 0;
				MDR_ld <= 1;
				R_W <= 0;
				MOV <= 0;
				MA1 <= 0;
				MA0 <= 0;
				MB1 <= 0;
				MB0 <= 1;
				MC1 <= 0;
				MC0 <= 0;
				MD <= 1;
				ME <= 0;
				OP4 <= 0;
				OP3 <= 0;
				OP2 <= (IR[20]) ? 1 : 0;
				OP1 <= (IR[20]) ? 0 : 1;
				OP0 <= 0;
              OP_CODE <= (IR[22])? `BYTE : `WORD;
			end
			8'd11:
			begin
				FR_ld <= 0;
				RF_ld <= 0;
				IR_ld <= 0;
				MAR_ld <= 0;
				MDR_ld <= 1;
				R_W <= 0;
				MOV <= 0;
				MA1 <= 0;
				MA0 <= 0;
				MB1 <= 0;
				MB0 <= 0;
				MC1 <= 0;
				MC0 <= 0;
				MD <= 1;
				ME <= 0;
				OP4 <= 0;
				OP3 <= 0;
				OP2 <= (IR[20]) ? 1 : 0;
				OP1 <= (IR[20]) ? 0 : 1;
				OP0 <= 0;
              OP_CODE <= (IR[22])? `BYTE : `WORD;
			end
			8'd12:
			begin
				FR_ld <= 0;
				RF_ld <= 0;
				IR_ld <= 0;
				MAR_ld <= 0;
				MDR_ld <= 1;
				R_W <= 0;
				MOV <= 0;
				MA1 <= 0;
				MA0 <= 0;
				MB1 <= 0;
				MB0 <= 0;
				MC1 <= 0;
				MC0 <= 0;
				MD <= 1;
				ME <= 0;
				OP4 <= 0;
				OP3 <= 0;
				OP2 <= (IR[20]) ? 1 : 0;
				OP1 <= (IR[20]) ? 0 : 1;
				OP0 <= 0;
              OP_CODE <= (IR[22])? `BYTE : `WORD;
			end
			8'd13:
			begin
				FR_ld <= 0;
				RF_ld <= 0;
				IR_ld <= 0;
				MAR_ld <= 0;
				MDR_ld <= 1;
				R_W <= 0;
				MOV <= 0;
				MA1 <= 0;
				MA0 <= 0;
				MB1 <= 0;
				MB0 <= 0;
				MC1 <= 0;
				MC0 <= 0;
				MD <= 1;
				ME <= 0;
				OP4 <= 0;
				OP2 <= (IR[20]) ? 1 : 0;
				OP1 <= (IR[20]) ? 0 : 1;
				OP1 <= 0;
				OP0 <= 0; OP_CODE <= (IR[22])? `BYTE : `WORD;
			end
			8'd14:
			begin
				FR_ld <= 0;
				RF_ld <= 0;
				IR_ld <= 0;
				MAR_ld <= 0;
				MDR_ld <= 1;
				R_W <= 0;
				MOV <= 0;
				MA1 <= 0;
				MA0 <= 0;
				MB1 <= 0;
				MB0 <= 1;
				MC1 <= 0;
				MC0 <= 0;
				MD <= 1;
				ME <= 0;
				OP4 <= 0;
				OP3 <= 0;
				OP2 <= (IR[20]) ? 1 : 0;
				OP1 <= (IR[20]) ? 0 : 1;
				OP0 <= 0; OP_CODE <= (IR[22])? `BYTE : `WORD;
			end
			8'd15:
			begin
				FR_ld <= 0;
				RF_ld <= 0;
				IR_ld <= 0;
				MAR_ld <= 0;
				MDR_ld <= 1;
				R_W <= 0;
				MOV <= 0;
				MA1 <= 0;
				MA0 <= 0;
				MB1 <= 0;
				MB0 <= 0;
				MC1 <= 0;
				MC0 <= 0;
				MD <= 1;
				ME <= 0;
				OP4 <= 0;
				OP3 <= 0;
				OP2 <= (IR[20]) ? 1 : 0;
				OP1 <= (IR[20]) ? 0 : 1;
				OP0 <= 0; OP_CODE <= (IR[22])? `BYTE : `WORD;
			end
			8'd16:
			begin
				FR_ld <= 0;
				RF_ld <= 0;
				IR_ld <= 0;
				MAR_ld <= 0;
				MDR_ld <= 1;
				R_W <= 0;
				MOV <= 0;
				MA1 <= 0;
				MA0 <= 0;
				MB1 <= 0;
				MB0 <= 1;
				MC1 <= 0;
				MC0 <= 0;
				MD <= 1;
				ME <= 0;
				OP4 <= 0;
				OP3 <= 0;
				OP2 <= (IR[20]) ? 1 : 0;
				OP1 <= (IR[20]) ? 0 : 1;
				OP0 <= 0; OP_CODE <= (IR[22])? `BYTE : `WORD;
			end
			8'd17:
			begin
				FR_ld <= 0;
				RF_ld <= 0;
				IR_ld <= 0;
				MAR_ld <= 0;
				MDR_ld <= 0;
				R_W <= 1;
				MOV <= 1;
				MA1 <= 0;
				MA0 <= 0;
				MB1 <= 0;
				MB0 <= 0;
				MC1 <= 0;
				MC0 <= 0;
				MD <= 0;
				ME <= 0;
				OP4 <= 0;
				OP3 <= 0;
				OP2 <= 0;
				OP1 <= 0;
				OP0 <= 0; OP_CODE <= (IR[22])? `BYTE : `WORD;
			end
			8'd18:
			begin
				FR_ld <= 0;
				RF_ld <= 0;
				IR_ld <= 0;
				MAR_ld <= 0;
				MDR_ld <= 0;
				R_W <= 1;
				MOV <= 1;
				MA1 <= 0;
				MA0 <= 0;
				MB1 <= 0;
				MB0 <= 0;
				MC1 <= 0;
				MC0 <= 0;
				MD <= 0;
				ME <= 0;
				OP4 <= 0;
				OP3 <= 0;
				OP2 <= 0;
				OP1 <= 0;
				OP0 <= 0; OP_CODE <= (IR[22])? `BYTE : `WORD;
			end
			8'd19:
			begin
				FR_ld <= 0;
				RF_ld <= 0;
				IR_ld <= 0;
				MAR_ld <= 0;
				MDR_ld <= 0;
				R_W <= 1;
				MOV <= 1;
				MA1 <= 0;
				MA0 <= 0;
				MB1 <= 0;
				MB0 <= 0;
				MC1 <= 0;
				MC0 <= 0;
				MD <= 0;
				ME <= 0;
				OP4 <= 0;
				OP3 <= 0;
				OP2 <= 0;
				OP1 <= 0;
				OP0 <= 0; OP_CODE <= (IR[22])? `BYTE : `WORD;
			end
			8'd20:
			begin
				FR_ld <= 0;
				RF_ld <= 0;
				IR_ld <= 0;
				MAR_ld <= 0;
				MDR_ld <= 0;
				R_W <= 1;
				MOV <= 1;
				MA1 <= 0;
				MA0 <= 0;
				MB1 <= 0;
				MB0 <= 0;
				MC1 <= 0;
				MC0 <= 0;
				MD <= 0;
				ME <= 0;
				OP4 <= 0;
				OP3 <= 0;
				OP2 <= 0;
				OP1 <= 0; OP_CODE <= (IR[22])? `BYTE : `WORD;
				OP0 <= 0;
			end
			8'd21:
			begin
				FR_ld <= 0;
				RF_ld <= 0;
				IR_ld <= 0;
				MAR_ld <= 0;
				MDR_ld <= 0;
				R_W <= 1;
				MOV <= 1;
				MA1 <= 0;
				MA0 <= 0;
				MB1 <= 0;
				MB0 <= 0;
				MC1 <= 0;
				MC0 <= 0;
				MD <= 0;
				ME <= 0;
				OP4 <= 0;
				OP3 <= 0;
				OP2 <= 0;
				OP1 <= 0;
				OP0 <= 0; OP_CODE <= (IR[22])? `BYTE : `WORD;
			end
			8'd22:
			begin
				FR_ld <= 0;
				RF_ld <= 0;
				IR_ld <= 0;
				MAR_ld <= 0;
				MDR_ld <= 0;
				R_W <= 1;
				MOV <= 1;
				MA1 <= 0;
				MA0 <= 0;
				MB1 <= 0;
				MB0 <= 0;
				MC1 <= 0;
				MC0 <= 0;
				MD <= 0;
				ME <= 0;
				OP4 <= 0;
				OP3 <= 0;
				OP2 <= 0;
				OP1 <= 0;
				OP0 <= 0; OP_CODE <= (IR[22])? `BYTE : `WORD;
			end
			8'd23:
			begin
				FR_ld <= 0;
				RF_ld <= 0;
				IR_ld <= 0;
				MAR_ld <= 0;
				MDR_ld <= 0;
				R_W <= 1;
				MOV <= 1;
				MA1 <= 0;
				MA0 <= 0;
				MB1 <= 0;
				MB0 <= 0;
				MC1 <= 0;
				MC0 <= 0;
				MD <= 0;
				ME <= 0;
				OP4 <= 0;
				OP3 <= 0;
				OP2 <= 0;
				OP1 <= 0;
				OP0 <= 0; OP_CODE <= (IR[22])? `BYTE : `WORD;
			end
			8'd24:
			begin
				FR_ld <= 0;
				RF_ld <= 0;
				IR_ld <= 0;
				MAR_ld <= 0;
				MDR_ld <= 0;
				R_W <= 1;
				MOV <= 1;
				MA1 <= 0;
				MA0 <= 0;
				MB1 <= 0;
				MB0 <= 0;
				MC1 <= 0;
				MC0 <= 0;
				MD <= 0;
				ME <= 0;
				OP4 <= 0;
				OP3 <= 0;
				OP2 <= 0;
				OP1 <= 0;
				OP0 <= 0; OP_CODE <= (IR[22])? `BYTE : `WORD;
			end
			8'd25:
			begin
				FR_ld <= 0;
				RF_ld <= 0;
				IR_ld <= 0;
				MAR_ld <= 0;
				MDR_ld <= 0;
				R_W <= 1;
				MOV <= 1;
				MA1 <= 0;
				MA0 <= 0;
				MB1 <= 0;
				MB0 <= 0;
				MC1 <= 0;
				MC0 <= 0;
				MD <= 0;
				ME <= 0;
				OP4 <= 0;
				OP3 <= 0;
				OP2 <= 0;
				OP1 <= 0;
				OP0 <= 0; OP_CODE <= (IR[22])? `BYTE : `WORD;
			end
			8'd26:
			begin
				FR_ld <= 0;
				RF_ld <= 0;
				IR_ld <= 0;
				MAR_ld <= 1;
				MDR_ld <= 0;
				R_W <= 1;
				MOV <= 1;
				MA1 <= 0;
				MA0 <= 0;
				MB1 <= 0;
				MB0 <= 0;
				MC1 <= 0;
				MC0 <= 0;
				MD <= 0;
				ME <= 0;
				OP4 <= 0;
				OP3 <= 0;
				OP2 <= 0;
				OP1 <= 0;
				OP0 <= 0; OP_CODE <= (IR[22])? `BYTE : `WORD;
			end
			8'd27:
			begin
				FR_ld <= 0;
				RF_ld <= 0;
				IR_ld <= 0;
				MAR_ld <= 1;
				MDR_ld <= 0;
				R_W <= 1;
				MOV <= 1;
				MA1 <= 0;
				MA0 <= 0;
				MB1 <= 0;
				MB0 <= 0;
				MC1 <= 0;
				MC0 <= 0;
				MD <= 0;
				ME <= 0;
				OP4 <= 0;
				OP3 <= 0;
				OP2 <= 0;
				OP1 <= 0;
				OP0 <= 0; OP_CODE <= (IR[22])? `BYTE : `WORD;
			end
			8'd28:
			begin
				FR_ld <= 0;
				RF_ld <= 0;
				IR_ld <= 0;
				MAR_ld <= 1;
				MDR_ld <= 0;
				R_W <= 1;
				MOV <= 1;
				MA1 <= 0;
				MA0 <= 0;
				MB1 <= 0;
				MB0 <= 0;
				MC1 <= 0;
				MC0 <= 0;
				MD <= 0;
				ME <= 0;
				OP4 <= 0;
				OP3 <= 0;
				OP2 <= 0;
				OP1 <= 0;
				OP0 <= 0; OP_CODE <= (IR[22])? `BYTE : `WORD;
			end
			8'd29:
			begin
				FR_ld <= 0;
				RF_ld <= 0;
				IR_ld <= 0;
				MAR_ld <= 1;
				MDR_ld <= 0;
				R_W <= 1;
				MOV <= 1;
				MA1 <= 0;
				MA0 <= 0;
				MB1 <= 0;
				MB0 <= 0;
				MC1 <= 0;
				MC0 <= 0;
				MD <= 0;
				ME <= 0;
				OP4 <= 0;
				OP3 <= 0;
				OP2 <= 0;
				OP1 <= 0;
				OP0 <= 0; OP_CODE <= (IR[22])? `BYTE : `WORD;
			end
			8'd30:
			begin
				FR_ld <= 0;
				RF_ld <= 0;
				IR_ld <= 0;
				MAR_ld <= 1;
				MDR_ld <= 0;
				R_W <= 1;
				MOV <= 1;
				MA1 <= 0;
				MA0 <= 0;
				MB1 <= 0;
				MB0 <= 0;
				MC1 <= 0;
				MC0 <= 0;
				MD <= 0;
				ME <= 0;
				OP4 <= 0;
				OP3 <= 0;
				OP2 <= 0;
				OP1 <= 0;
				OP0 <= 0; OP_CODE <= (IR[22])? `BYTE : `WORD;
			end
			8'd31:
			begin
				FR_ld <= 0;
				RF_ld <= 0;
				IR_ld <= 0;
				MAR_ld <= 1;
				MDR_ld <= 0;
				R_W <= 1;
				MOV <= 1;
				MA1 <= 0;
				MA0 <= 0;
				MB1 <= 0;
				MB0 <= 0;
				MC1 <= 0;
				MC0 <= 0;
				MD <= 0;
				ME <= 0;
				OP4 <= 0;
				OP3 <= 0;
				OP2 <= 0;
				OP1 <= 0;
				OP0 <= 0; OP_CODE <= (IR[22])? `BYTE : `WORD;
			end
			8'd32:
			begin
				FR_ld <= 0;
				RF_ld <= 0;
				IR_ld <= 0;
				MAR_ld <= 1;
				MDR_ld <= 0;
				R_W <= 1;
				MOV <= 1;
				MA1 <= 0;
				MA0 <= 0;
				MB1 <= 0;
				MB0 <= 0;
				MC1 <= 0;
				MC0 <= 0;
				MD <= 0;
				ME <= 0;
				OP4 <= 0;
				OP3 <= 0;
				OP2 <= 0;
				OP1 <= 0; OP_CODE <= (IR[22])? `BYTE : `WORD;
				OP0 <= 0;
			end
			8'd33:
			begin
				FR_ld <= 0;
				RF_ld <= 0;
				IR_ld <= 0;
				MAR_ld <= 1;
				MDR_ld <= 0;
				R_W <= 1;
				MOV <= 1;
				MA1 <= 0;
				MA0 <= 0;
				MB1 <= 0;
				MB0 <= 0;
				MC1 <= 0;
				MC0 <= 0;
				MD <= 0;
				ME <= 0;
				OP4 <= 0;
				OP3 <= 0;
				OP2 <= 0;
				OP1 <= 0;
				OP0 <= 0; OP_CODE <= (IR[22])? `BYTE : `WORD;
			end
			8'd34:
			begin
				FR_ld <= 0;
				RF_ld <= 0;
				IR_ld <= 0;
				MAR_ld <= 1;
				MDR_ld <= 0;
				R_W <= 1;
				MOV <= 1;
				MA1 <= 0;
				MA0 <= 0;
				MB1 <= 0;
				MB0 <= 0;
				MC1 <= 0;
				MC0 <= 0;
				MD <= 0;
				ME <= 0;
				OP4 <= 0;
				OP3 <= 0;
				OP2 <= 0;
				OP1 <= 0;
				OP0 <= 0; OP_CODE <= (IR[22])? `BYTE : `WORD;
			end
			8'd35:
			begin
				FR_ld <= 0;
				RF_ld <= 0;
				IR_ld <= 0;
				MAR_ld <= 0;
				MDR_ld <= 0;
				R_W <= 0;
				MOV <= 0;
				MA1 <= 0;
				MA0 <= 0;
				MB1 <= 1;
				MB0 <= 0;
				MC1 <= 0;
				MC0 <= 0;
				MD <= 1;
				ME <= 0;
				OP4 <= 0;
				OP3 <= 1;
				OP2 <= 1;
				OP1 <= 0;
				OP0 <= 1; OP_CODE <= (IR[22])? `BYTE : `WORD;
			end
			8'd36:
			begin
				FR_ld <= 0;
				RF_ld <= 0;
				IR_ld <= 0;
				MAR_ld <= 0;
				MDR_ld <= 0;
				R_W <= 0;
				MOV <= 0;
				MA1 <= 0;
				MA0 <= 0;
				MB1 <= 1;
				MB0 <= 0;
				MC1 <= 0;
				MC0 <= 0;
				MD <= 1;
				ME <= 0;
				OP4 <= 0;
				OP3 <= 1;
				OP2 <= 1;
				OP1 <= 0; OP_CODE <= (IR[22])? `BYTE : `WORD;
				OP0 <= 1;
			end
			8'd37:
			begin
				FR_ld <= 0;
				RF_ld <= 0;
				IR_ld <= 0;
				MAR_ld <= 0;
				MDR_ld <= 0;
				R_W <= 0;
				MOV <= 0;
				MA1 <= 0;
				MA0 <= 0;
				MB1 <= 1;
				MB0 <= 0;
				MC1 <= 0;
				MC0 <= 0;
				MD <= 1;
				ME <= 0;
				OP4 <= 0;
				OP3 <= 1;
				OP2 <= 1;
				OP1 <= 0; OP_CODE <= (IR[22])? `BYTE : `WORD;
				OP0 <= 1;
			end
			8'd38:
			begin
				FR_ld <= 0;
				RF_ld <= 0;
				IR_ld <= 0;
				MAR_ld <= 0;
				MDR_ld <= 0;
				R_W <= 0;
				MOV <= 0;
				MA1 <= 0;
				MA0 <= 0;
				MB1 <= 1;
				MB0 <= 0;
				MC1 <= 0;
				MC0 <= 0;
				MD <= 1;
				ME <= 0;
				OP4 <= 0;
				OP3 <= 1;
				OP2 <= 1; OP_CODE <= (IR[22])? `BYTE : `WORD;
				OP1 <= 0;
				OP0 <= 1;
			end
			8'd39:
			begin
				FR_ld <= 0;
				RF_ld <= 0;
				IR_ld <= 0;
				MAR_ld <= 0;
				MDR_ld <= 0;
				R_W <= 0;
				MOV <= 0;
				MA1 <= 0;
				MA0 <= 0;
				MB1 <= 1;
				MB0 <= 0;
				MC1 <= 0;
				MC0 <= 0;
				MD <= 1;
				ME <= 0;
				OP4 <= 0;
				OP3 <= 1;
				OP2 <= 1;
				OP1 <= 0; OP_CODE <= (IR[22])? `BYTE : `WORD;
				OP0 <= 1;
			end
			8'd40:
			begin
				FR_ld <= 0;
				RF_ld <= 0;
				IR_ld <= 0;
				MAR_ld <= 0;
				MDR_ld <= 0;
				R_W <= 0;
				MOV <= 0;
				MA1 <= 0;
				MA0 <= 0;
				MB1 <= 1;
				MB0 <= 0;
				MC1 <= 0;
				MC0 <= 0;
				MD <= 1;
				ME <= 0;
				OP4 <= 0;
				OP3 <= 1;
				OP2 <= 1;
				OP1 <= 0; OP_CODE <= (IR[22])? `BYTE : `WORD;
				OP0 <= 1;
			end
			8'd41:
			begin
				FR_ld <= 0;
				RF_ld <= 0;
				IR_ld <= 0;
				MAR_ld <= 0;
				MDR_ld <= 0;
				R_W <= 0;
				MOV <= 0;
				MA1 <= 0;
				MA0 <= 0;
				MB1 <= 1;
				MB0 <= 0;
				MC1 <= 0;
				MC0 <= 0;
				MD <= 1;
				ME <= 0;
				OP4 <= 0;
				OP3 <= 1; OP_CODE <= (IR[22])? `BYTE : `WORD;
				OP2 <= 1;
				OP1 <= 0;
				OP0 <= 1;
			end
			8'd42:
			begin
				FR_ld <= 0;
				RF_ld <= 0;
				IR_ld <= 0;
				MAR_ld <= 0;
				MDR_ld <= 0;
				R_W <= 0;
				MOV <= 0;
				MA1 <= 0;
				MA0 <= 0;
				MB1 <= 1;
				MB0 <= 0;
				MC1 <= 0;
				MC0 <= 0;
				MD <= 1;
				ME <= 0;
				OP4 <= 0;
				OP3 <= 1;
				OP2 <= 1; OP_CODE <= (IR[22])? `BYTE : `WORD;
				OP1 <= 0;
				OP0 <= 1;
			end
			8'd43:
			begin
				FR_ld <= 0;
				RF_ld <= 0;
				IR_ld <= 0;
				MAR_ld <= 0;
				MDR_ld <= 0;
				R_W <= 0;
				MOV <= 0;
				MA1 <= 0;
				MA0 <= 0;
				MB1 <= 1;
				MB0 <= 0;
				MC1 <= 0;
				MC0 <= 0;
				MD <= 1;
				ME <= 0;
				OP4 <= 0;
				OP3 <= 1;
				OP2 <= 1;
				OP1 <= 0; OP_CODE <= (IR[22])? `BYTE : `WORD;
				OP0 <= 1;
			end
			8'd44:
			begin
				FR_ld <= 0;
				RF_ld <= 1;
				IR_ld <= 0;
				MAR_ld <= 0;
				MDR_ld <= 0;
				R_W <= 0;
				MOV <= 0;
				MA1 <= 0;
				MA0 <= 0;
				MB1 <= 0;
				MB0 <= 1;
				MC1 <= 1;
				MC0 <= 0;
				MD <= 1;
				ME <= 0;
				OP4 <= 0;
				OP3 <= 0;
				OP2 <= (IR[20]) ? 1 : 0;
				OP1 <= (IR[20]) ? 0 : 1;
				OP0 <= 0; OP_CODE <= (IR[22])? `BYTE : `WORD;
			end
			8'd45:
			begin
				FR_ld <= 0;
				RF_ld <= 1;
				IR_ld <= 0;
				MAR_ld <= 0;
				MDR_ld <= 0;
				R_W <= 0;
				MOV <= 0;
				MA1 <= 0;
				MA0 <= 0;
				MB1 <= 0;
				MB0 <= 1;
				MC1 <= 1;
				MC0 <= 0;
				MD <= 1;
				ME <= 0;
				OP4 <= 0;
				OP3 <= 1;
				OP2 <= 1;
				OP1 <= 0;
				OP0 <= 1; OP_CODE <= (IR[22])? `BYTE : `WORD;
			end
			8'd46:
			begin
				FR_ld <= 0;
				RF_ld <= 1;
				IR_ld <= 0;
				MAR_ld <= 0;
				MDR_ld <= 0;
				R_W <= 0;
				MOV <= 0;
				MA1 <= 0;
				MA0 <= 0;
				MB1 <= 0;
				MB0 <= 0;
				MC1 <= 1;
				MC0 <= 0;
				MD <= 1;
				ME <= 0;
				OP4 <= 0;
				OP3 <= 1;
				OP2 <= 1;
				OP1 <= 0; OP_CODE <= (IR[22])? `BYTE : `WORD;
				OP0 <= 1;
			end
			8'd47:
			begin
				FR_ld <= 0;
				RF_ld <= 1;
				IR_ld <= 0;
				MAR_ld <= 0;
				MDR_ld <= 0;
				R_W <= 0;
				MOV <= 0;
				MA1 <= 0;
				MA0 <= 0;
				MB1 <= 0;
				MB0 <= 0;
				MC1 <= 1;
				MC0 <= 0;
				MD <= 1;
				ME <= 0;
				OP4 <= 0;
				OP3 <= 1;
				OP2 <= 1;
				OP1 <= 0; OP_CODE <= (IR[22])? `BYTE : `WORD;
				OP0 <= 1;
			end
			8'd48:
			begin
				FR_ld <= 0;
				RF_ld <= 1;
				IR_ld <= 0;
				MAR_ld <= 0;
				MDR_ld <= 0;
				R_W <= 0;
				MOV <= 0;
				MA1 <= 0;
				MA0 <= 0;
				MB1 <= 0;
				MB0 <= 1;
				MC1 <= 1;
				MC0 <= 0;
				MD <= 1;
				ME <= 0;
				OP4 <= 0;
				OP3 <= 1;
				OP2 <= 1;
				OP1 <= 0;
				OP0 <= 1; OP_CODE <= (IR[22])? `BYTE : `WORD;
			end
			8'd49:
			begin
				FR_ld <= 0;
				RF_ld <= 1;
				IR_ld <= 0;
				MAR_ld <= 0;
				MDR_ld <= 0;
				R_W <= 0;
				MOV <= 0;
				MA1 <= 0;
				MA0 <= 0;
				MB1 <= 0;
				MB0 <= 1;
				MC1 <= 1;
				MC0 <= 0;
				MD <= 1;
				ME <= 0;
				OP4 <= 0;
				OP3 <= 1;
				OP2 <= 1;
				OP1 <= 0; OP_CODE <= (IR[22])? `BYTE : `WORD;
				OP0 <= 1;
			end
			8'd50:
			begin
				FR_ld <= 0;
				RF_ld <= 0;
				IR_ld <= 0;
				MAR_ld <= 0;
				MDR_ld <= 1;
				R_W <= 0;
				MOV <= 0;
				MA1 <= 0;
				MA0 <= 0;
				MB1 <= 0;
				MB0 <= 1;
				MC1 <= 0;
				MC0 <= 0;
				MD <= 1;
				ME <= 0;
				OP4 <= 0; OP_CODE <= (IR[22])? `BYTE : `WORD;
				OP3 <= 0;
				OP2 <= (IR[20]) ? 1 : 0;
				OP1 <= (IR[20]) ? 0 : 1;
				OP0 <= 0;
			end
			8'd51:
			begin
				FR_ld <= 0;
				RF_ld <= 0;
				IR_ld <= 0;
				MAR_ld <= 0;
				MDR_ld <= 1;
				R_W <= 0;
				MOV <= 0;
				MA1 <= 0;
				MA0 <= 0;
				MB1 <= 0;
				MB0 <= 0;
				MC1 <= 0;
				MC0 <= 0;
				MD <= 1;
				ME <= 0;
				OP4 <= 0;
				OP3 <= 0; OP_CODE <= (IR[22])? `BYTE : `WORD;
				OP2 <= (IR[20]) ? 1 : 0;
				OP1 <= (IR[20]) ? 0 : 1;
				OP0 <= 0;
			end
			8'd52:
			begin
				FR_ld <= 0;
				RF_ld <= 0;
				IR_ld <= 0;
				MAR_ld <= 0;
				MDR_ld <= 1;
				R_W <= 0;
				MOV <= 0;
				MA1 <= 0;
				MA0 <= 0;
				MB1 <= 0;
				MB0 <= 1;
				MC1 <= 0;
				MC0 <= 0;
				MD <= 1;
				ME <= 0;
				OP4 <= 0;
				OP3 <= 0; OP_CODE <= (IR[22])? `BYTE : `WORD;
				OP2 <= (IR[20]) ? 1 : 0;
				OP1 <= (IR[20]) ? 0 : 1;
				OP0 <= 0;
			end
			8'd53:
			begin
				FR_ld <= 0;
				RF_ld <= 0;
				IR_ld <= 0;
				MAR_ld <= 0;
				MDR_ld <= 1;
				R_W <= 0;
				MOV <= 0;
				MA1 <= 0;
				MA0 <= 0;
				MB1 <= 0;
				MB0 <= 0;
				MC1 <= 0;
				MC0 <= 0;
				MD <= 1;
				ME <= 0;
				OP4 <= 0;
				OP3 <= 0; OP_CODE <= (IR[22])? `BYTE : `WORD;
				OP2 <= (IR[20]) ? 1 : 0;
				OP1 <= (IR[20]) ? 0 : 1;
				OP0 <= 0;
			end
			8'd54:
			begin
				FR_ld <= 0;
				RF_ld <= 0;
				IR_ld <= 0;
				MAR_ld <= 0;
				MDR_ld <= 1;
				R_W <= 0;
				MOV <= 0;
				MA1 <= 0;
				MA0 <= 0;
				MB1 <= 0;
				MB0 <= 0;
				MC1 <= 0;
				MC0 <= 0;
				MD <= 1;
				ME <= 0; OP_CODE <= (IR[22])? `BYTE : `WORD;
				OP4 <= 0;
				OP3 <= 0;
				OP2 <= (IR[20]) ? 1 : 0;
				OP1 <= (IR[20]) ? 0 : 1;
				OP0 <= 0;
			end
			8'd55:
			begin
				FR_ld <= 0;
				RF_ld <= 0;
				IR_ld <= 0;
				MAR_ld <= 0;
				MDR_ld <= 1;
				R_W <= 0;
				MOV <= 0;
				MA1 <= 0;
				MA0 <= 0;
				MB1 <= 0;
				MB0 <= 0;
				MC1 <= 0;
				MC0 <= 0;
				MD <= 1;
				ME <= 0;
				OP4 <= 0; OP_CODE <= (IR[22])? `BYTE : `WORD;
				OP3 <= 0;
				OP2 <= (IR[20]) ? 1 : 0;
				OP1 <= (IR[20]) ? 0 : 1;
				OP0 <= 0;
			end
			8'd56:
			begin
				FR_ld <= 0;
				RF_ld <= 0;
				IR_ld <= 0;
				MAR_ld <= 0;
				MDR_ld <= 1;
				R_W <= 0;
				MOV <= 0;
				MA1 <= 0;
				MA0 <= 0;
				MB1 <= 0;
				MB0 <= 1;
				MC1 <= 0;
				MC0 <= 0;
				MD <= 1; OP_CODE <= (IR[22])? `BYTE : `WORD;
				ME <= 0;
				OP4 <= 0;
				OP3 <= 0;
				OP2 <= (IR[20]) ? 1 : 0;
				OP1 <= (IR[20]) ? 0 : 1;
				OP0 <= 0;
			end
			8'd57:
			begin
				FR_ld <= 0;
				RF_ld <= 0;
				IR_ld <= 0;
				MAR_ld <= 0;
				MDR_ld <= 1;
				R_W <= 0;
				MOV <= 0;
				MA1 <= 0;
				MA0 <= 0;
				MB1 <= 0;
				MB0 <= 0;
				MC1 <= 0;
				MC0 <= 0;
				MD <= 1;
				ME <= 0;
				OP4 <= 0; OP_CODE <= (IR[22])? `BYTE : `WORD;
				OP3 <= 0;
				OP2 <= (IR[20]) ? 1 : 0;
				OP1 <= (IR[20]) ? 0 : 1;
				OP0 <= 0;
			end
			8'd58:
			begin
				FR_ld <= 0;
				RF_ld <= 0;
				IR_ld <= 0;
				MAR_ld <= 0;
				MDR_ld <= 1;
				R_W <= 0;
				MOV <= 0;
				MA1 <= 0;
				MA0 <= 0;
				MB1 <= 0;
				MB0 <= 1;
				MC1 <= 0; OP_CODE <= (IR[22])? `BYTE : `WORD;
				MC0 <= 0;
				MD <= 1;
				ME <= 0;
				OP4 <= 0;
				OP3 <= 0;
				OP2 <= (IR[20]) ? 1 : 0;
				OP1 <= (IR[20]) ? 0 : 1;
				OP0 <= 0;
			end
			8'd59:
			begin
				FR_ld <= 0;
				RF_ld <= 0;
				IR_ld <= 0;
				MAR_ld <= 1;
				MDR_ld <= 0;
				R_W <= 0;
				MOV <= 0;
				MA1 <= 0;
				MA0 <= 1;
				MB1 <= 0;
				MB0 <= 0;
				MC1 <= 0;
				MC0 <= 0;
				MD <= 0;
				ME <= 0;
				OP4 <= 1;
				OP3 <= 0;
				OP2 <= 0; OP_CODE <= (IR[22])? `BYTE : `WORD;
				OP1 <= 0;
				OP0 <= 0;
			end
			8'd60:
			begin
				FR_ld <= 0;
				RF_ld <= 0;
				IR_ld <= 0;
				MAR_ld <= 1;
				MDR_ld <= 0;
				R_W <= 0;
				MOV <= 0;
				MA1 <= 0;
				MA0 <= 1;
				MB1 <= 0;
				MB0 <= 0;
				MC1 <= 0;
				MC0 <= 0; OP_CODE <= (IR[22])? `BYTE : `WORD;
				MD <= 0;
				ME <= 0;
				OP4 <= 1;
				OP3 <= 0;
				OP2 <= 0;
				OP1 <= 0;
				OP0 <= 0;
			end
			8'd61:
			begin
				FR_ld <= 0;
				RF_ld <= 0;
				IR_ld <= 0;
				MAR_ld <= 1;
				MDR_ld <= 0;
				R_W <= 0;
				MOV <= 0;
				MA1 <= 0;
				MA0 <= 1;
				MB1 <= 0;
				MB0 <= 0;
				MC1 <= 0;
				MC0 <= 0;
				MD <= 0; OP_CODE <= (IR[22])? `BYTE : `WORD;
				ME <= 0;
				OP4 <= 1;
				OP3 <= 0;
				OP2 <= 0;
				OP1 <= 0;
				OP0 <= 0;
			end
			8'd62:
			begin
				FR_ld <= 0;
				RF_ld <= 0;
				IR_ld <= 0;
				MAR_ld <= 1;
				MDR_ld <= 0;
				R_W <= 0;
				MOV <= 0;
				MA1 <= 0;
				MA0 <= 1;
				MB1 <= 0;
				MB0 <= 0;
				MC1 <= 0;
				MC0 <= 0;
				MD <= 0;
				ME <= 0;
				OP4 <= 1;
				OP3 <= 0; OP_CODE <= (IR[22])? `BYTE : `WORD;
				OP2 <= 0;
				OP1 <= 0;
				OP0 <= 0;
			end
			8'd63:
			begin
				FR_ld <= 0;
				RF_ld <= 0;
				IR_ld <= 0;
				MAR_ld <= 1;
				MDR_ld <= 0;
				R_W <= 0;
				MOV <= 0;
				MA1 <= 0;
				MA0 <= 1;
				MB1 <= 0;
				MB0 <= 0;
				MC1 <= 0;
				MC0 <= 0;
				MD <= 0;
				ME <= 0;
				OP4 <= 1;
				OP3 <= 0;
				OP2 <= 0; OP_CODE <= (IR[22])? `BYTE : `WORD;
				OP1 <= 0;
				OP0 <= 0;
			end
			8'd64:
			begin
				FR_ld <= 0;
				RF_ld <= 0;
				IR_ld <= 0;
				MAR_ld <= 1;
				MDR_ld <= 0;
				R_W <= 0;
				MOV <= 0;
				MA1 <= 0;
				MA0 <= 1;
				MB1 <= 0;
				MB0 <= 0;
				MC1 <= 0;
				MC0 <= 0;
				MD <= 0;
				ME <= 0;
				OP4 <= 1; OP_CODE <= (IR[22])? `BYTE : `WORD;
				OP3 <= 0;
				OP2 <= 0;
				OP1 <= 0;
				OP0 <= 0;
			end
			8'd65:
			begin
				FR_ld <= 0;
				RF_ld <= 0;
				IR_ld <= 0;
				MAR_ld <= 1;
				MDR_ld <= 0;
				R_W <= 0;
				MOV <= 0;
				MA1 <= 0;
				MA0 <= 1;
				MB1 <= 0;
				MB0 <= 0;
				MC1 <= 0;
				MC0 <= 0;
				MD <= 0;
				ME <= 0;
				OP4 <= 1;
				OP3 <= 0; OP_CODE <= (IR[22])? `BYTE : `WORD;
				OP2 <= 0;
				OP1 <= 0;
				OP0 <= 0;
			end
			8'd66:
			begin
				FR_ld <= 0;
				RF_ld <= 0;
				IR_ld <= 0;
				MAR_ld <= 1;
				MDR_ld <= 0;
				R_W <= 0;
				MOV <= 0;
				MA1 <= 0;
				MA0 <= 1;
				MB1 <= 0;
				MB0 <= 0;
				MC1 <= 0;
				MC0 <= 0;
				MD <= 0;
				ME <= 0;
				OP4 <= 1; OP_CODE <= (IR[22])? `BYTE : `WORD;
				OP3 <= 0;
				OP2 <= 0;
				OP1 <= 0;
				OP0 <= 0;
			end
			8'd67:
			begin
				FR_ld <= 0;
				RF_ld <= 0;
				IR_ld <= 0;
				MAR_ld <= 1;
				MDR_ld <= 0;
				R_W <= 0;
				MOV <= 0;
				MA1 <= 0;
				MA0 <= 1;
				MB1 <= 0;
				MB0 <= 0;
				MC1 <= 0;
				MC0 <= 0;
				MD <= 0; OP_CODE <= (IR[22])? `BYTE : `WORD;
				ME <= 0;
				OP4 <= 1;
				OP3 <= 0;
				OP2 <= 0;
				OP1 <= 0;
				OP0 <= 0;
			end
			8'd68:
			begin
				FR_ld <= 0;
				RF_ld <= 0;
				IR_ld <= 0;
				MAR_ld <= 0;
				MDR_ld <= 0;
				R_W <= 0;
				MOV <= 1;
				MA1 <= 0;
				MA0 <= 0;
				MB1 <= 0;
				MB0 <= 0;
				MC1 <= 0;
				MC0 <= 0;
				MD <= 0;
				ME <= 0; OP_CODE <= (IR[22])? `BYTE : `WORD;
				OP4 <= 0;
				OP3 <= 0;
				OP2 <= 0;
				OP1 <= 0;
				OP0 <= 0;
			end
			8'd69:
			begin
				FR_ld <= 0;
				RF_ld <= 0;
				IR_ld <= 0;
				MAR_ld <= 0;
				MDR_ld <= 0;
				R_W <= 0;
				MOV <= 1;
				MA1 <= 0;
				MA0 <= 0;
				MB1 <= 0;
				MB0 <= 0;
				MC1 <= 0;
				MC0 <= 0;
				MD <= 0; OP_CODE <= (IR[22])? `BYTE : `WORD;
				ME <= 0;
				OP4 <= 0;
				OP3 <= 0;
				OP2 <= 0;
				OP1 <= 0;
				OP0 <= 0;
			end
			8'd70:
			begin
				FR_ld <= 0;
				RF_ld <= 0;
				IR_ld <= 0;
				MAR_ld <= 0;
				MDR_ld <= 0;
				R_W <= 0;
				MOV <= 1;
				MA1 <= 0;
				MA0 <= 0;
				MB1 <= 0;
				MB0 <= 0;
				MC1 <= 0;
				MC0 <= 0;
				MD <= 0;
				ME <= 0; OP_CODE <= (IR[22])? `BYTE : `WORD;
				OP4 <= 0;
				OP3 <= 0;
				OP2 <= 0;
				OP1 <= 0;
				OP0 <= 0;
			end
			8'd71:
			begin
				FR_ld <= 0;
				RF_ld <= 0;
				IR_ld <= 0;
				MAR_ld <= 0;
				MDR_ld <= 0;
				R_W <= 0;
				MOV <= 1;
				MA1 <= 0;
				MA0 <= 0;
				MB1 <= 0;
				MB0 <= 0;
				MC1 <= 0;
				MC0 <= 0;
				MD <= 0;
				ME <= 0;
				OP4 <= 0; OP_CODE <= (IR[22])? `BYTE : `WORD;
				OP3 <= 0;
				OP2 <= 0;
				OP1 <= 0;
				OP0 <= 0;
			end
			8'd72:
			begin
				FR_ld <= 0;
				RF_ld <= 0;
				IR_ld <= 0;
				MAR_ld <= 0;
				MDR_ld <= 0;
				R_W <= 0;
				MOV <= 1;
				MA1 <= 0;
				MA0 <= 0;
				MB1 <= 0;
				MB0 <= 0;
				MC1 <= 0;
				MC0 <= 0;
				MD <= 0;
				ME <= 0;
				OP4 <= 0;
				OP3 <= 0; OP_CODE <= (IR[22])? `BYTE : `WORD;
				OP2 <= 0;
				OP1 <= 0;
				OP0 <= 0;
			end
			8'd73:
			begin
				FR_ld <= 0;
				RF_ld <= 0;
				IR_ld <= 0;
				MAR_ld <= 0;
				MDR_ld <= 0;
				R_W <= 0;
				MOV <= 1;
				MA1 <= 0;
				MA0 <= 0;
				MB1 <= 0;
				MB0 <= 0;
				MC1 <= 0;
				MC0 <= 0;
				MD <= 0; OP_CODE <= (IR[22])? `BYTE : `WORD;
				ME <= 0;
				OP4 <= 0;
				OP3 <= 0;
				OP2 <= 0;
				OP1 <= 0;
				OP0 <= 0;
			end
			8'd74:
			begin
				FR_ld <= 0;
				RF_ld <= 0;
				IR_ld <= 0;
				MAR_ld <= 0;
				MDR_ld <= 0;
				R_W <= 0;
				MOV <= 1;
				MA1 <= 0;
				MA0 <= 0;
				MB1 <= 0; OP_CODE <= (IR[22])? `BYTE : `WORD;
				MB0 <= 0;
				MC1 <= 0;
				MC0 <= 0;
				MD <= 0;
				ME <= 0;
				OP4 <= 0;
				OP3 <= 0;
				OP2 <= 0;
				OP1 <= 0;
				OP0 <= 0;
			end
			8'd75:
			begin
				FR_ld <= 0;
				RF_ld <= 0;
				IR_ld <= 0;
				MAR_ld <= 0;
				MDR_ld <= 0;
				R_W <= 0;
				MOV <= 1; OP_CODE <= (IR[22])? `BYTE : `WORD;
				MA1 <= 0;
				MA0 <= 0;
				MB1 <= 0;
				MB0 <= 0;
				MC1 <= 0;
				MC0 <= 0;
				MD <= 0;
				ME <= 0;
				OP4 <= 0;
				OP3 <= 0;
				OP2 <= 0;
				OP1 <= 0;
				OP0 <= 0;
			end
			8'd76:
			begin
				FR_ld <= 0;
				RF_ld <= 0;
				IR_ld <= 0;
				MAR_ld <= 0;
				MDR_ld <= 0;
				R_W <= 0;
				MOV <= 1;
				MA1 <= 0;
				MA0 <= 0;
				MB1 <= 0;
				MB0 <= 0;
				MC1 <= 0;
				MC0 <= 0;
				MD <= 0;
				ME <= 0;
				OP4 <= 0; OP_CODE <= (IR[22])? `BYTE : `WORD;
				OP3 <= 0;
				OP2 <= 0;
				OP1 <= 0;
				OP0 <= 0;
			end
			8'd77:
			begin
				FR_ld <= 0;
				RF_ld <= 0;
				IR_ld <= 0;
				MAR_ld <= 0;
				MDR_ld <= 0;
				R_W <= 0;
				MOV <= 1;
				MA1 <= 0;
				MA0 <= 0;
				MB1 <= 0;
				MB0 <= 0;
				MC1 <= 0;
				MC0 <= 0;
				MD <= 0;
				ME <= 0;
				OP4 <= 0;
				OP3 <= 0;
				OP2 <= 0;
				OP1 <= 0; OP_CODE <= (IR[22])? `BYTE : `WORD;
				OP0 <= 0;
			end
			8'd78:
			begin
				FR_ld <= 0;
				RF_ld <= 0;
				IR_ld <= 0;
				MAR_ld <= 0;
				MDR_ld <= 0;
				R_W <= 0;
				MOV <= 1;
				MA1 <= 0;
				MA0 <= 0;
				MB1 <= 0;
				MB0 <= 0;
				MC1 <= 0;
				MC0 <= 0;
				MD <= 0;
				ME <= 0;
				OP4 <= 0; OP_CODE <= (IR[22])? `BYTE : `WORD;
				OP3 <= 0;
				OP2 <= 0;
				OP1 <= 0;
				OP0 <= 0;
			end
			8'd79:
			begin
				FR_ld <= 0;
				RF_ld <= 0;
				IR_ld <= 0;
				MAR_ld <= 0;
				MDR_ld <= 0;
				R_W <= 0;
				MOV <= 1;
				MA1 <= 0;
				MA0 <= 0;
				MB1 <= 0;
				MB0 <= 0;
				MC1 <= 0;
				MC0 <= 0; OP_CODE <= (IR[22])? `BYTE : `WORD;
				MD <= 0;
				ME <= 0;
				OP4 <= 0;
				OP3 <= 0;
				OP2 <= 0;
				OP1 <= 0;
				OP0 <= 0;
			end
			8'd80:
			begin
				FR_ld <= 0;
				RF_ld <= 0;
				IR_ld <= 0;
				MAR_ld <= 0;
				MDR_ld <= 0;
				R_W <= 0;
				MOV <= 1;
				MA1 <= 0;
				MA0 <= 0;
				MB1 <= 0;
				MB0 <= 0;
				MC1 <= 0;
				MC0 <= 0;
				MD <= 0;
				ME <= 0;
				OP4 <= 0;
				OP3 <= 0;
				OP2 <= 0;
				OP1 <= 0; OP_CODE <= (IR[22])? `BYTE : `WORD;
				OP0 <= 0;
			end
			8'd81:
			begin
				FR_ld <= 0;
				RF_ld <= 0;
				IR_ld <= 0;
				MAR_ld <= 0;
				MDR_ld <= 0;
				R_W <= 0;
				MOV <= 1;
				MA1 <= 0;
				MA0 <= 0;
				MB1 <= 0;
				MB0 <= 0;
				MC1 <= 0;
				MC0 <= 0;
				MD <= 0;
				ME <= 0;
				OP4 <= 0;
				OP3 <= 0;
				OP2 <= 0; OP_CODE <= (IR[22])? `BYTE : `WORD;
				OP1 <= 0;
				OP0 <= 0;
			end
			8'd82:
			begin
				FR_ld <= 0;
				RF_ld <= 0;
				IR_ld <= 0;
				MAR_ld <= 0;
				MDR_ld <= 0;
				R_W <= 0;
				MOV <= 1;
				MA1 <= 0;
				MA0 <= 0;
				MB1 <= 0;
				MB0 <= 0;
				MC1 <= 0;
				MC0 <= 0;
				MD <= 0;
				ME <= 0;
				OP4 <= 0;
				OP3 <= 0;
				OP2 <= 0;
				OP1 <= 0; OP_CODE <= (IR[22])? `BYTE : `WORD;
				OP0 <= 0;
			end
			8'd83:
			begin
				FR_ld <= 0;
				RF_ld <= 0;
				IR_ld <= 0;
				MAR_ld <= 0;
				MDR_ld <= 0;
				R_W <= 0;
				MOV <= 1;
				MA1 <= 0;
				MA0 <= 0;
				MB1 <= 0;
				MB0 <= 0;
				MC1 <= 0;
				MC0 <= 0;
				MD <= 0;
				ME <= 0;
				OP4 <= 0;
				OP3 <= 0;
				OP2 <= 0; OP_CODE <= (IR[22])? `BYTE : `WORD;
				OP1 <= 0;
				OP0 <= 0;
			end
			8'd84:
			begin
				FR_ld <= 0;
				RF_ld <= 0;
				IR_ld <= 0;
				MAR_ld <= 0;
				MDR_ld <= 0;
				R_W <= 0;
				MOV <= 1;
				MA1 <= 0;
				MA0 <= 0;
				MB1 <= 0;
				MB0 <= 0;
				MC1 <= 0;
				MC0 <= 0;
				MD <= 0;
				ME <= 0;
				OP4 <= 0;
				OP3 <= 0;
				OP2 <= 0;
				OP1 <= 0; OP_CODE <= (IR[22])? `BYTE : `WORD;
				OP0 <= 0;
			end
			8'd85:
			begin
				FR_ld <= 0;
				RF_ld <= 0;
				IR_ld <= 0;
				MAR_ld <= 0;
				MDR_ld <= 0;
				R_W <= 0;
				MOV <= 1;
				MA1 <= 0;
				MA0 <= 0;
				MB1 <= 0;
				MB0 <= 0;
				MC1 <= 0;
				MC0 <= 0;
				MD <= 0;
				ME <= 0;
				OP4 <= 0;
				OP3 <= 0; OP_CODE <= (IR[22])? `BYTE : `WORD;
				OP2 <= 0;
				OP1 <= 0;
				OP0 <= 0;
			end
			8'd86:
			begin
				FR_ld <= 0;
				RF_ld <= 1;
				IR_ld <= 0;
				MAR_ld <= 0;
				MDR_ld <= 0;
				R_W <= 0;
				MOV <= 0;
				MA1 <= 0;
				MA0 <= 0;
				MB1 <= 0;
				MB0 <= 1;
				MC1 <= 1;
				MC0 <= 0;
				MD <= 1; OP_CODE <= (IR[22])? `BYTE : `WORD;
				ME <= 0;
				OP4 <= 0;
				OP3 <= 0;
				OP2 <= (IR[20]) ? 1 : 0;
				OP1 <= (IR[20]) ? 0 : 1;
				OP0 <= 0;
			end
			8'd87:
			begin
				FR_ld <= 0;
				RF_ld <= 1;
				IR_ld <= 0;
				MAR_ld <= 0;
				MDR_ld <= 0;
				R_W <= 0;
				MOV <= 0;
				MA1 <= 0;
				MA0 <= 0;
				MB1 <= 0;
				MB0 <= 1;
				MC1 <= 1;
				MC0 <= 0;
				MD <= 1;
				ME <= 0;
				OP4 <= 0;
				OP3 <= 0;
				OP2 <= (IR[20]) ? 1 : 0;
				OP1 <= (IR[20]) ? 0 : 1;
				OP0 <= 0;
			end
			8'd88:
			begin
				FR_ld <= 0;
				RF_ld <= 1;
				IR_ld <= 0;
				MAR_ld <= 0;
				MDR_ld <= 0;
				R_W <= 0;
				MOV <= 0;
				MA1 <= 0;
				MA0 <= 0;
				MB1 <= 0;
				MB0 <= 0;
				MC1 <= 1;
				MC0 <= 0;
				MD <= 1;
				ME <= 0;
				OP4 <= 0;
				OP3 <= 0;
				OP2 <= (IR[20]) ? 1 : 0;
				OP1 <= (IR[20]) ? 0 : 1;
				OP0 <= 0;
			end
			8'd89:
			begin
				FR_ld <= 0;
				RF_ld <= 1;
				IR_ld <= 0;
				MAR_ld <= 0;
				MDR_ld <= 0;
				R_W <= 0;
				MOV <= 0;
				MA1 <= 0;
				MA0 <= 0;
				MB1 <= 0;
				MB0 <= 0;
				MC1 <= 1;
				MC0 <= 0;
				MD <= 1;
				ME <= 0;
				OP4 <= 0;
				OP3 <= 0;
				OP2 <= (IR[20]) ? 1 : 0;
				OP1 <= (IR[20]) ? 0 : 1;
				OP0 <= 0;
			end
			8'd90:
			begin
				FR_ld <= 0;
				RF_ld <= 1;
				IR_ld <= 0;
				MAR_ld <= 0;
				MDR_ld <= 0;
				R_W <= 0;
				MOV <= 0;
				MA1 <= 0;
				MA0 <= 0;
				MB1 <= 0;
				MB0 <= 1;
				MC1 <= 1;
				MC0 <= 0;
				MD <= 1;
				ME <= 0;
				OP4 <= 0;
				OP3 <= 0;
				OP2 <= (IR[20]) ? 1 : 0;
				OP1 <= (IR[20]) ? 0 : 1;
				OP0 <= 0;
			end
			8'd91:
			begin
				FR_ld <= 0;
				RF_ld <= 1;
				IR_ld <= 0;
				MAR_ld <= 0;
				MDR_ld <= 0;
				R_W <= 0;
				MOV <= 0;
				MA1 <= 0;
				MA0 <= 0;
				MB1 <= 0;
				MB0 <= 1;
				MC1 <= 1;
				MC0 <= 0;
				MD <= 1;
				ME <= 0;
				OP4 <= 0;
				OP3 <= 0;
				OP2 <= (IR[20]) ? 1 : 0;
				OP1 <= (IR[20]) ? 0 : 1;
				OP0 <= 0;
			end
			8'd92:
			begin
				FR_ld <= 0;
				RF_ld <= 1;
				IR_ld <= 0;
				MAR_ld <= 0;
				MDR_ld <= 0;
				R_W <= 0;
				MOV <= 0;
				MA1 <= 1;
				MA0 <= 0;
				MB1 <= 0;
				MB0 <= 1;
				MC1 <= 0;
				MC0 <= 1;
				MD <= 1;
				ME <= 0;
				OP4 <= 0;
				OP3 <= 0;
				OP2 <= 1;
				OP1 <= 0;
				OP0 <= 0;
			end
			8'd93:
			begin
				FR_ld <= 0;
				RF_ld <= 1;
				IR_ld <= 0;
				MAR_ld <= 0;
				MDR_ld <= 0;
				R_W <= 0;
				MOV <= 0;
				MA1 <= 0;
				MA0 <= 0;
				MB1 <= 0;
				MB0 <= 1;
				MC1 <= 0;
				MC0 <= 0;
				MD <= 0;
				ME <= 0;
				OP4 <= 0;
				OP3 <= 0;
				OP2 <= 0;
				OP1 <= 0;
				OP0 <= 0;
			end
			8'd94:
			begin
				FR_ld <= 1;
				RF_ld <= 0;
				IR_ld <= 0;
				MAR_ld <= 0;
				MDR_ld <= 0;
				R_W <= 0;
				MOV <= 0;
				MA1 <= 0;
				MA0 <= 0;
				MB1 <= 0;
				MB0 <= 1;
				MC1 <= 0;
				MC0 <= 0;
				MD <= 0;
				ME <= 0;
				OP4 <= 0;
				OP3 <= 0;
				OP2 <= 0;
				OP1 <= 0;
				OP0 <= 0;
			end
          8'd200:begin
            	FR_ld <= 0;
				RF_ld <= 1;
				IR_ld <= 0;
				MAR_ld <= 0;
				MDR_ld <= 0;
				R_W <= 0;
				MOV <= 0;
				MA1 <= 1;
				MA0 <= 0;
				MB1 <= 0;
				MB0 <= 0;
				MC1 <= 1;
				MC0 <= 1;
				MD <= 1;
				ME <= 0;
				OP4 <= 1;
				OP3 <= 0;
				OP2 <= 0;
				OP1 <= 0;
				OP0 <= 0;
          end
		endcase
		
		// $display("IR_Ld<=%b  R/W<=%b  MOV<=%b  ", IR_ld , R_W , MOV);

		end
endmodule

module StateReg(output reg[7:0]State, input[7:0] NextState, input Clr, Clk);

	initial State <= 0;
    always @(posedge Clk)

    if(!Clr)
	begin
       State <= 8'b00000;
	    // $display("State: %d",State);
	end
    else
	begin
       State <= NextState;
	    // $display("State: %d  Clock: %d     Time:%d  ",State  , Clk, $time);
	end
    
endmodule

// module CU_tester;
    
// 	reg[4:0] OPs;
//     wire FR,RF, IR_ld ,MDR,MAR,R_W,MOV,MA_1,MA_0,MB_1,MB_0,MC_1,MC_0,MD, ME, OP4,OP3,OP2,OP1,OP0;
//     reg Moc, Clk, Clr, Cond;
// 	reg[31:0] IR;

// 	parameter sim_time <=1000;

//     ControlUnit cu( FR,RF,MDR,MAR,R_W,MOV,MA_1,MA_0,MB_1,MB_0,MC_1,MC_0,MD, ME, OP4,OP3,OP2,OP1,OP0,IR_ld ,IR,  Moc, Cond, Clr, Clk );
	

// 		initial begin


// 		Clk <= 1'b0;
// 		Clr <= 1'b1;
// 		Cond <= 1'b1;
// 		Moc <= 1'b1;
// 		IR <= 32'b1110_0100_1000_0001_0000_0000_0001_0000;

// 		#5
// 		begin
// 			repeat(100) #5 Clk<=~Clk;
// 		end
// 	end

// endmodule



