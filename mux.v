module mux32bits(input S0 , S1 , input[31:0] o0 , o1 , o2 , o3 , output reg[31:0] O );
// initial O = 32'd512;
always @*
begin
  case(S1)

    1'b0:begin
        O = (S0 == 0) ? o0 : o1;

    end

    1'b1:begin
        O = (S0 == 0) ? o2 : o3;

    end
  

  endcase
//  $display("Time: %d  32bits Selection: %b%b  o1: %b   o2: %b   o3: %b  o4: %b out: %b", $time , S0 , S1 , o0, o1 , o2 , o3 , O);

end

endmodule

module mux4bits(input S0 , S1 , input[3:0] o0 , o1 , o2 , o3 , output reg[3:0] O );
// initial O = 4'd13;
always @*
begin
  case(S1)

    1'b0:begin
        O = (S0 == 0) ? o0 : o1;

    end

    1'b1:begin
        O = (S0 == 0) ? o2 : o3;

    end


  endcase
          // $display("Time: %d  4bits Selection: %b%b  o1: %b   o2: %b   o3: %b  o4: %b out: %b", $time , S0 , S1 , o0, o1 , o2 , o3 , O);
end

endmodule

module mux5bits(input S0 ,  input[4:0] o0 , o1  ,output reg[4:0] O );
// initial O = 5'd10;
always @*
begin
  

  // O <= 5'b10001;
  case(S0)

    1'b0:begin
        O <= o0 ;

    end

    1'b1:begin
        O <= o1;

    end


  endcase

      // $display("OPbits Selection: %b  o1: %b   o2: %b   out: %b", S0, o0, o1 , O);
end

endmodule

// module test();
// reg[4:0] o1 , o2 , o3 , o4;
// reg s0 , s1;
// wire [4:0] sel; 
// mux5bits mux(s0 , s1 , o1 , o2 ,o3 , o4 , sel);



// initial begin
//   s0 = 1'b0;
//   s1 = 1'b0;
//   o1 = 5'd1;
//   o2 = 5'd2;
//   o3 = 5'd3;
//   o4 = 5'd4;

//   // repeat(100) #5
//   begin
//     #5 s0 = 1'b1;
//     #10 begin
//       s1 = 1'b1;
//       s0 = 1'b0;
//     end

//     #15 begin
//     s1 = 1'b1;
//     s0 = 1'b1;
//     end
    
//   end
//   begin
//   $display("%b %b%b", sel , s1 , s0);
//   end

// end



// endmodule