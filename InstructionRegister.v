module IR(input[31:0]Ds , input Ld , Clk , output reg[31:0]Qs);

    initial Qs <= 32'd0;
    always @(posedge Clk , Ld)
    begin
        if(Ld)
        begin
        Qs <= Ds;
        // $display("IReg  IR: %b" , Qs);
        end
    end


endmodule