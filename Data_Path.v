`include "ALU_32.v" 
`include "mux.v"
`include "PSR.v"
`include "RAM.v"
`include "RegisterFile.v"
`include "MAR.v"
`include "MDR.v"
`include "shift.v"
`include "CondTester.v"
`include "InstructionRegister.v"
`include "dummyRegFile.v"
`include "freg.v"

module DataPath(
    input FR,RF, MDR,MAR,R_W,MOV,MA_1,MA_0,MB_1,MB_0,
    MC_1,MC_0,MD, ME, OP4,OP3,OP2,OP1,OP0,IR_ld,output [31:0] instructionRegister,
  output  Moc, output wire cond , Clr, Clk  , input[1:0] OP_CODE
    
);

wire N , Z , V , C;
wire  Nin , Zin , Cin , Vin ;
wire[3:0] Asel , Bsel , Csel; // ,reg selection
wire[31:0]  A , PB , out , sOut , B;
parameter[3:0] PC  = 4'b1111;
parameter zero32 = 32'b0000;
parameter some32 = 32'd4096;
parameter zero4 = 4'b0000;
parameter zero5 = 5'b0000;
wire[31:0] MEx;
wire[31:0]dataIn , dataOut;
wire[7:0] address;
wire[4:0] OPs;
wire[31:0] instructionRegister;
wire[1:0] mode; 

wire[4:0] OPCU , OPIR;


assign OPCU = {OP4 , OP3 , OP2 ,OP1 , OP0};
assign OPIR = { 1'b0 , instructionRegister[24:21] };

assign Bsel = instructionRegister[3:0];

//---------------Muxes-----------------------------
mux4bits muxA(MA_0 , MA_1 , instructionRegister[19:16] , instructionRegister[15:12] , PC , zero4 , Asel);
mux32bits muxB(MB_0 , MB_1 , PB ,sOut , dataIn, zero32 , B );
  mux4bits muxC(MC_0 , MC_1 , instructionRegister[15:12] , PC , instructionRegister[19:16] , 4'd14  , Csel);
mux5bits muxD(MD ,   OPIR  ,OPCU ,OPs);
mux32bits muxE(ME , 1'b0 , dataOut , out , zero32 , zero32 , MEx);


//----------------Components----------------
IR InstructionRegister(dataOut , IR_ld , Clk ,instructionRegister );
// regfile registerFile(out  , Csel , Asel , Bsel , RF , Clr , Clk , A , PB);
register_file freg(PB , A , out  , Clk  , RF , Csel , Asel , instructionRegister[3:0]);
PSR psr(Clk , Nin , Zin , Cin  , Vin , FR , N , Z , V , C );
ALU alu(Nin , Zin , Vin , Cin , out , C , A , B , OPs[4:0] );
MAR mar(out , MAR , Clk , address ); // padded with 0s
MDR mdr(MEx , MDR , Clk , dataIn);
//Shifter shifter(sOut , C , instructionRegister , PB);
  Shifter s(sOut , Cin , C , instructionRegister , PB ); 
ConditionTester tester(N , Z , V, C , instructionRegister[31:28] , cond);
  ram_interact ram(dataIn , address, R_W , MOV , OP_CODE , Moc , dataOut , instructionRegister);

// always@*

// begin
// //   $display("Data Path: %b    %b",OPs , OP0);
// end

// always@*
// begin
//   $display("OPS CU: %b   IR: %b " , {OP0 , OP1 , OP2 , OP3 , OP4} , {1'b0 ,instructionRegister[24:21] } );
// end

//     integer file_in, file_out, file_status, i;
//     reg [31:0] data;
// 	reg enable, w_r;
//     reg [31:0] data_in;
// 	reg [7:0] addresser;
//     // reg[1:0] mode;

//     wire [31:0] data_output;
// 	wire MOC;
   
//    always@(Clk)
//     if(Clk == 1)
//     begin
// 		file_in = $fopen("data_bin.txt","rb");
//         //file_in = $fopen("data.txt","r");
//         w_r = 1'b0;
//         // mode = `WORD;
// 		addresser =  8'b00000000;
// 		while (!$feof(file_in)) begin
// 			file_status = $fscanf(file_in, "%b", data);
//             //file_status = $fread(data, file_in);
//             //$readmemb(file_in, data);
// 			ram.storage[address] = data;
// 			// $display("Memory address: %h \n Data: %b", address, data);
// 			addresser = addresser + 1;
// 		end
// 		$fclose(file_in);
// 	end




endmodule




