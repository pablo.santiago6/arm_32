
module ALU(output reg N, Z, V, Cout,  output reg  [31:0]O, input Cin , input  [31:0]A, B , input [4:0]OP);

  reg og_sign , shouldbe_sign ,ovfl_sign,flagUpdate;
  // initial N = 1'b0;
  // initial Z = 1'b0;
  // initial V = 1'b0;
  // initial Cout = 1'b0;
  // initial O = 32'b0;

  reg[4:0] internal;

  always @*
  begin 
    internal <= OP;
  
    // $display("Time: %d ALU: A: %d  B: %d  out:%d  Ops: %b   NZVC : %b%b%b%b   ",$time , A ,B ,O , OP , N , V , Z , Cout);
  case (internal)
  
    5'b00000:  // And
      begin
        O <= A && B;
      end
    
    5'b00001: //Xor
      begin
        O <= A^^B; 
      end
    
    5'b00010: // A - B
      begin
        {Cout , O} <= {1'b0,A} - {1'b0,B};
      end

    5'b00011: // B - A
      begin
        {Cout , O} <= {1'b0,B} - {1'b0,A};
      end
    
    5'b00100: // A + B
      begin
        {Cout , O }  <= A  +  B ;
      end

    5'b00101: // A + B + Cin
      begin
        {Cout , O} <=  (Cin == 1) ? (A + B + 1):(A+B); 
      end

    5'b00110: // A - B - (not Cin)
      begin
        {Cout , O} <= (Cin == 1) ? ({1'b0,A} - {1'b0,B} - {32'h1}) : ({1'b0,A} - {1'b0,B});
      end

    5'b00111:  //B - A - ( not Cin)
      begin
        {Cout , O} <= (Cin == 1) ? ({1'b0,B} - {1'b0,A} - {32'h1}) : ({1'b0,B} - {1'b0,A});
      end
    
    5'b01000:  // And
      begin
        flagUpdate <= 1'b1;
        O <= A && B;
      end
    
    5'b01001: //Xor
      begin
        flagUpdate <= 1'b1;
        O <= A^^B; 
      end
    
    5'b01010: // A - B
      begin
        flagUpdate <= 1'b1;
        {Cout , O} <= {1'b0,A} - {1'b0,B};
      end

    5'b01011: // A + B
      begin
        flagUpdate <= 1'b1;
        {Cout , O }  <= A  +  B ;
      end
    
    5'b01100: // A or B
      begin
        flagUpdate <= 1'b1;
        O <= A ^^ B;
      end

    // 5'b01101: // A + B + Cin
    //   begin
    //     flagUpdate <= 1'b1;
    //     {Cout , O} <=  (Cin == 1) ? (A + B + 1):(A+B); 
    //   end

    // 5'b01110: // A - B - (not Cin)
    //   begin
    //     flagUpdate <= 1'b1;
    //     {Cout , O} <= (Cin == 1) ? ({1'b0,A} - {1'b0,B} - {32'h1}) : ({1'b0,A} - {1'b0,B});
    //   end

    // 5'b01111:  //B - A - ( not Cin)
    //   begin
    //     flagUpdate <= 1'b1;
    //     {Cout , O} <= (Cin == 1) ? ({1'b0,B} - {1'b0,A} - {32'h1}) : ({1'b0,B} - {1'b0,A});
    //   end

    // 5'b01100: // OR
    //   begin
    //     O <= A || B ;
    //   end
    
    5'b01101: //Return B
      begin
        // $display("01101");
        O <= B;
      end
    
    5'b01110: // A and ( not B)
      begin
        O <= A && ~B;
      end
    
    5'b01111: //not B
      begin
        O <= ~B;
      end
    
    5'b10000: //return A
      begin
        // $display("10000");
        O <= A;
      end
    
    5'b10001: //return A + 4
      begin
        // $display("10001");
        O  <= A + 32'd4;
      end
   
    5'b10010: //return A + B + 4
      begin
        {Cout , O }<= A + B + 32'd4;
      end
    
   // default:begin
      //$display("OPCODE %b has no match",OP);
  //  end

    
  endcase


  if(flagUpdate)
    begin
      Z = ( O == 32'b0) ? 1:0;
      
      N = (O[0] === 1'b1)? 1:0;



      begin
          og_sign <= (A[0]);
          
          // shouldbe_sign<=
          //     (((OP==4'b1010 || OP == 4'b1000)&&(A[0] != B[0]))//if a sub between different signs 
          //     ||((OP==4'b1001 || OP==4'b0111)&&(A[0] == B[0]))); // if sum and both signs are the same 
              
          ovfl_sign<=(((OP==4'b1010 || OP == 4'b1000)&&(A[0] != B[0]))
              ||((OP==4'b1001 || OP==4'b0111)&&(A[0] == B[0])) );
      end

    // N = O[0] ^ ((shouldbe_sign)&&(og_sign != O[0]));
       V = (ovfl_sign)&&(og_sign != O[0]);
    end
   
    
end

endmodule

// module test();

// wire N , Z , V , C ;
// wire[31:0] O;

// reg Cin;

// reg[31:0] A , B;
// reg[4:0] Ops;

// ALU aLU(N , Z  , V , C , O , Cin , A , B , Ops);

// initial begin
//   A = 32'd0;
//   B = 32'd4;
//   Ops = 5'b10001;

//   // repeat(40)#5 Ops = Ops + 5'b1;

//   // #5$display(O)

// end

// initial begin
//   $monitor("OP: %b  out: %d",Ops , O);
// end

// endmodule
