`define LSL 2'b00
`define LSR 2'b01
`define ASR 2'b01
`define ROR 2'b11;
    /**
    This module takes the IR to find out which type of shift is to be done and 
    on what number it should be done.

    ASR 
    Arithmetic shift right by n bits moves the left-hand 32-n bits of the register Rm,
     to the right by n places, into the right-hand 32-n bits of the result. And it copies the original bit[31] 
     of the register into the left-hand n bits of the result

    LSR
    Logical shift right by n bits moves the left-hand 32-n bits of the register Rm, to the right by n places, 
    into the right-hand 32-n bits of the result. And it sets the left-hand n bits of the result to 0.
    
    LSL
    Logical shift left by n bits moves the right-hand 32-n bits of the register Rm, to the left by n places, into the left-hand 32-n bits of the result. 
    And it sets the right-hand n bits of the result to 0

    ROR
    Rotate right by n bits moves the left-hand 32-n bits of the register Rm, to the right by n places, into the right-hand 32-n bits of the result.
     And it moves the right-hand n bits of the register into the left-hand n bits of the result. 
    
    **/
module Shifter(output reg [31:0] operand, output reg Cout, input Cin, input[31:0] IR, Rm);
 reg [31:0] temp;
 reg [29:0] temp2;
 reg MSB;
 integer positions, rotate_imm, index;

 always @(IR) begin
     // initializing Cout
     Cout = Cin;
     case (IR[27:25])
         3'b000: // shift by immediate -----------------------------------------------------
          begin
             if (IR[4] == 0) begin
               case (IR[6:5])
                   2'b00:
                   begin
                     if (IR[11:7] == 5'b00000)
                        operand = Rm;
                     else begin
                         positions = IR[11:7];
                         temp = Rm; 
                         for(index = 0; index < positions; index = index + 1) begin
                             operand = temp << 1;
                         end
                         Cout = (IR[20] == 1'b1)? operand[31] : Cin;
                     end
                   end 

                   2'b01:
                   begin
                     positions = IR[11:7];
                     temp = Rm;
                     for (index = 0; index < positions; index = index + 1)
                        temp = temp>>1;
                     Cout = (IR[20] == 1'b1)? temp[31] : Cin;
                     operand = temp;
                   end

                   2'b11: // ROR
                   begin
                     if (IR[11:7] == 5'b00000)
                        begin
                        temp = Rm;
                        temp = temp>>1;
                        temp[31] = Cin;
                        end
                      else begin
                          temp = Rm;
                          positions = IR[11:7];
                          for(index = 0; index < positions; index = index + 1)begin
                              MSB = temp[0];
                              temp = temp>>1;
                              temp[31] = MSB;
                          end
                          Cout = (IR[20] == 1'b1)? temp[31] : Cin;
                          operand = temp;
                      end
                    end    
                  endcase
               end
            if (IR[4] == 1 && IR[7] == 1) begin // Miscellaneous L/S -----------------------------------------------------
                 temp = 32'b0;
                 temp[7:0] = {IR[11:8], IR[3:0]};
                 operand = temp;
            end   
          end
           
           3'b001: // 32-bit immediate -----------------------------------------------------
           begin
               temp = 32'b0;
               temp[7:0] = IR[7:0];
               rotate_imm = IR[11:8];
               positions = 2 * rotate_imm;
               for (index = 0; index < positions; index = index + 1 ) begin
                   MSB = temp[0];
                   temp = temp >> 1;
                   temp[31] = MSB;
               end
               Cout = (IR[20] == 1)? temp[31]:Cin;
               operand = temp;
           end

           3'b010: begin // immediate offset L/S -----------------------------------------------------
               temp = 32'b0;
               temp[11:0] = IR[11:0];
               operand = temp;
           end

           3'b101: begin // branch -----------------------------------------------------
              {Cout, operand} = {{6{IR[23]}}, IR[23:0]} << 2;
           end
         
     endcase
 end
endmodule
