module MAR(input[31:0]Ds , input Ld , Clk , output reg[7:0]Qs);
    initial Qs <= 8'b0;
    always @(posedge Clk , Ld)
    begin
        if(Ld)begin
         
        Qs <= Ds[7:0];
        // $display("MAR Ds: %b" , Ds);

        end
    end


endmodule